package rla.actions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.actions.debugactions.DebugDestroyWorld;
import rla.actions.debugactions.DebugSummonGoblin;
import rla.actions.debugactions.DebugTestInventory;
import rla.actions.debugactions.DebugWorldMoveAction;
import rla.actions.inventorystateactions.ChangePageAction;
import rla.actions.inventorystateactions.SelectItemAction;
import rla.actions.lookstateactions.LookByAction;
import rla.actions.playstateactions.*;
import rla.states.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Actions {
    private Map<Class<? extends GameState>, Map<InputType, Action>> actionMap;
    private Map<Class<? extends GameState>, Map<Character, Action>> charActionMap;
    private static Actions singletonActions;

    private Actions() {

    }

    public static Actions getActions() {
        if(singletonActions == null) {
            singletonActions = new Actions();
        }
        return singletonActions;
    }

    public Optional<Action> getAction(Class<? extends GameState> state, KeyStroke input) {
        if(input.inputType() == InputType.Character) {
            return Optional.ofNullable(charActionMap.get(state).get(input.getCharacter()));
        }
        return Optional.ofNullable(actionMap.get(state).get(input.inputType()));
    }

    public static Action getInvalidAction() {
        return new InvalidAction();
    }

    public void initializeActions() {
        initializeActionMaps();
        /* Register actions here */
        registerAction(PlayState.class, InputType.ArrowUp, new MoveAction(0, -1));
        registerAction(PlayState.class, InputType.ArrowDown, new MoveAction(0, 1));
        registerAction(PlayState.class, InputType.ArrowLeft, new MoveAction(-1, 0));
        registerAction(PlayState.class, InputType.ArrowRight, new MoveAction(1, 0));
        registerAction(PlayState.class, InputType.F1, new DebugWorldMoveAction());
        registerAction(PlayState.class, InputType.Enter, new WaitAction());
        registerAction(PlayState.class, InputType.F2, new DebugSummonGoblin());
        registerAction(PlayState.class, InputType.F3, new DebugTestInventory());
        registerAction(PlayState.class, InputType.F4, new DebugDestroyWorld());
        registerCharAction(PlayState.class, 'D', new DigAction());
        registerCharAction(PlayState.class, 'd', new DropItemAction());
        registerCharAction(PlayState.class, 'g', new PickItemAction());
        registerCharAction(PlayState.class, 'e', new EatAction());
        registerCharAction(PlayState.class, 'l', new LookAction());
        registerCharAction(PlayState.class, 'q', new QuaffAction());
        registerCharAction(PlayState.class, 't', new UseOnGroundAction());
        registerCharAction(PlayState.class, 'w', new WearAction());

        // DropScreen actions
        registerInventoryBasicActions(InventoryDropState.class);

        registerInventoryBasicActions(InventoryPickState.class);

        //Eat screen actions
        registerInventoryBasicActions(InventoryEatState.class);

        //Quaff screen actions
        registerInventoryBasicActions(InventoryQuaffState.class);

        // Use state
        registerInventoryBasicActions(InventoryUseState.class);

        // Wear state
        registerInventoryBasicActions((InventoryWearState.class));

        // BodyWear state
        registerInventoryBasicActions(BodyWearState.class);

        // Look screen
        registerAction(LookState.class, InputType.ArrowUp, new LookByAction(0, -1));
        registerAction(LookState.class, InputType.ArrowDown, new LookByAction(0, 1));
        registerAction(LookState.class, InputType.ArrowLeft, new LookByAction(-1, 0));
        registerAction(LookState.class, InputType.ArrowRight, new LookByAction(1, 0));
        registerAction(LookState.class, InputType.Escape, new WaitAction());


    }

    public void initializeActionMaps() {
        actionMap = new HashMap<>();
        charActionMap = new HashMap<>();
        // Initializing playstate actions
        actionMap.put(PlayState.class, new HashMap<>());
        charActionMap.put(PlayState.class, new HashMap<>());
        // Drop state
        actionMap.put(InventoryDropState.class, new HashMap<>());
        charActionMap.put(InventoryDropState.class, new HashMap<>());
        // Pick state
        actionMap.put(InventoryPickState.class, new HashMap<>());
        charActionMap.put(InventoryPickState.class, new HashMap<>());
        // eat state
        actionMap.put(InventoryEatState.class, new HashMap<>());
        charActionMap.put(InventoryEatState.class, new HashMap<>());
        // look state
        actionMap.put(LookState.class, new HashMap<>());
        charActionMap.put(LookState.class, new HashMap<>());
        // quaff state
        actionMap.put(InventoryQuaffState.class, new HashMap<>());
        charActionMap.put(InventoryQuaffState.class, new HashMap<>());
        // useonGround state
        actionMap.put(InventoryUseState.class, new HashMap<>());
        charActionMap.put(InventoryUseState.class, new HashMap<>());
        // Wear state
        actionMap.put(InventoryWearState.class, new HashMap<>());
        charActionMap.put(InventoryWearState.class, new HashMap<>());

        // BodyWear state
        actionMap.put(BodyWearState.class, new HashMap<>());
        charActionMap.put(BodyWearState.class, new HashMap<>());
    }

    private void registerInventoryBasicActions(Class<? extends GameState> state) {
        for(char c='a';c<'z';c++) {
            registerCharAction(state, c, new SelectItemAction());
        }
        registerAction(state, InputType.ArrowLeft, new ChangePageAction(-1));
        registerAction(state, InputType.ArrowRight, new ChangePageAction(1));
        registerAction(state, InputType.Escape, new WaitAction());
    }

    public void registerAction(Class<? extends GameState> state, InputType input, Action action) {
        Map<InputType, Action> stateMap = actionMap.get(state);
        stateMap.put(input, action);
    }

    public void registerCharAction(Class<? extends GameState> state, Character c, Action action) {
        Map<Character, Action> stateMap = charActionMap.get(state);
        stateMap.put(c, action);
    }



}
