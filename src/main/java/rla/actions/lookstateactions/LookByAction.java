package rla.actions.lookstateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.LookState;

public class LookByAction implements Action {
    int x;
    int y;

    public LookByAction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean perform(GameState state) {
        ((LookState)state).move(x, y);
        return false;
    }
}
