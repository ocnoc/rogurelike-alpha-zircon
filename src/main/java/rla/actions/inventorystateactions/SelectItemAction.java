package rla.actions.inventorystateactions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.actions.Action;
import rla.states.GameState;
import rla.states.InventoryState;
import rla.states.SelectableList;

public class SelectItemAction implements Action {
    @Override
    public boolean perform(GameState state) {
        return false;
    }

    @Override
    public boolean perform(GameState state, KeyStroke input) {
        int index = input.getCharacter() - 'a';
        ((SelectableList)state).use(index);
        return false;
    }

    @Override
    public boolean isMultiStep() {
        return true;
    }
}
