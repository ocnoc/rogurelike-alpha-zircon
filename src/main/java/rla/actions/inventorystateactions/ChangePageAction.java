package rla.actions.inventorystateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.InventoryState;
import rla.states.SelectableList;

public class ChangePageAction implements Action {
    int i;

    public ChangePageAction(int i) {
        this.i = i;
    }

    @Override
    public boolean perform(GameState state) {
        ((SelectableList)state).changePage(i);
        return false;
    }
}
