package rla.actions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.states.GameState;

public interface Action {
    /**
     * Performs the given action. Usually linked to a specific keystroke
     * @param state The game state we're modifying with the action, usually the state we call it from
     * @return whether the action was successful. Used for deciding whether to continue turn in PlayState
     */
    boolean perform(GameState state); // single-key action

    /**
     * Some actions are "multistep", and thus take two inputs, such as dig ('d' to dig, then direction). This is the second
     * perform function, used for sending an action its second input
     * @param state state being affected
     * @param input Second key being pressed, delivered directly to action.
     * @return Whether the action completed successfully
     */
    default boolean perform(GameState state, KeyStroke input) {return false;} // two-key action

    /**
     * A check if the action is a multistep, so the game state knows whether to keep it for another input.
     * @return if the action is multistep
     */
    default boolean isMultiStep() { return false; }

    /**
     * Transition states are used by screen-opening actions to open another state ontop of a previous.
     * Examples would be inventory opening actions.
     * @return the state to transition to upon action completion
     */
    default GameState getTransitionState() { return null; }
}
