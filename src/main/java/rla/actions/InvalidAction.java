package rla.actions;

import org.hexworks.zircon.api.input.InputType;
import rla.states.GameState;

public class InvalidAction implements Action {
    @Override
    public boolean perform(GameState state) {
        return false;
    }

    @Override
    public boolean isMultiStep() {
        return false;
    }
}
