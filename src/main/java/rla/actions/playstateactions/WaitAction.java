package rla.actions.playstateactions;

import org.hexworks.zircon.api.input.InputType;
import rla.actions.Action;
import rla.states.GameState;

public class WaitAction implements Action {
    @Override
    public boolean perform(GameState state) {
        return true;
    }
}
