package rla.actions.playstateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.InventoryDropState;
import rla.states.PlayState;

public class DropItemAction implements Action {
    GameState transitionState = null;
    @Override
    public boolean perform(GameState state) {
        transitionState = new InventoryDropState(((PlayState)state).getPlayer(),
                ((PlayState)state).getPlayer().getInventory(),
                state,
                ((PlayState)state).getPlayScreen());
        return true;
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }
}
