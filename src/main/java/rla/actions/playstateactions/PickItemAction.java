package rla.actions.playstateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.InventoryPickState;
import rla.states.PlayState;

public class PickItemAction implements Action {
    GameState transitionState = null;
    @Override
    public boolean perform(GameState state) {
        transitionState = null;
        PlayState s = ((PlayState)state);
        if(s.getPlayer().getCurrentTile().getInventory().isEmpty()) {
            s.getPlayer().notify("There's nothing there.");
            return false;
        }
        else {
            transitionState = new InventoryPickState(s.getPlayer(), s.getPlayer().getCurrentTile().getInventory(), state, s.getPlayScreen());
            return true;
        }
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }
}
