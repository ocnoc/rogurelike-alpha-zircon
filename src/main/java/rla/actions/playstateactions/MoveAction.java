package rla.actions.playstateactions;

import org.hexworks.zircon.api.input.InputType;
import rla.actions.Action;
import rla.entities.actors.Actor;
import rla.states.GameState;
import rla.states.PlayState;

public class MoveAction implements Action {
    int xMovement;
    int yMovement;

    public MoveAction(int x, int y) {
        xMovement = x;
        yMovement = y;
    }


    @Override
    public boolean perform(GameState state) {
        Actor player = ((PlayState)state).getPlayer();
        return player.moveBy(xMovement, yMovement);
    }

    @Override
    public boolean isMultiStep() {
        return false;
    }

}
