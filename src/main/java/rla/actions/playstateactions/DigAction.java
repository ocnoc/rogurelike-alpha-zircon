package rla.actions.playstateactions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.actions.Action;
import rla.entities.actors.Actor;
import rla.states.GameState;
import rla.states.PlayState;

public class DigAction implements Action {
    @Override
    public boolean perform(GameState state) {
        ((PlayState)state).getPlayer().notify("Which direction?");
        return false;
    }

    @Override
    public boolean perform(GameState state, KeyStroke input) {
        InputType key = input.inputType();
        Actor player = ((PlayState)state).getPlayer();
        if(key == InputType.ArrowUp) {
            return player.dig(0, -1);
        }
        if(key == InputType.ArrowDown) {
            return player.dig(0, 1);
        }
        if(key == InputType.ArrowRight) {
            return player.dig(1, 0);
        }
        if(key == InputType.ArrowLeft) {
            return player.dig(-1, 0);
        }
        return false;
    }

    @Override
    public boolean isMultiStep() {
        return true;
    }
}
