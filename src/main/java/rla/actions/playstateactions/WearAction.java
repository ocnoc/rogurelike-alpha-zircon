package rla.actions.playstateactions;

import rla.actions.Action;
import rla.states.BodyWearState;
import rla.states.GameState;
import rla.states.PlayState;

public class WearAction implements Action {
    GameState transitionState;
    @Override
    public boolean perform(GameState state) {
        transitionState = new BodyWearState(((PlayState)state).getPlayer(), ((PlayState)state).getPlayScreen());
        return false;
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }
}
