package rla.actions.playstateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.LookState;
import rla.states.PlayState;

public class LookAction implements Action {
    GameState transitionState;
    @Override
    public boolean perform(GameState state) {
        transitionState = new LookState(((PlayState)state).getPlayer(),
                state,
                ((PlayState)state).getPlayScreen());
        return false;
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }
}
