package rla.actions.playstateactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.InventoryEatState;
import rla.states.PlayState;

public class EatAction implements Action {
    GameState transitionState = null;
    @Override
    public boolean perform(GameState state) {
        transitionState = new InventoryEatState(((PlayState)state).getPlayer(),
                ((PlayState)state).getPlayer().getInventory(),
                state,
                ((PlayState)state).getPlayScreen());
        return true;
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }
}
