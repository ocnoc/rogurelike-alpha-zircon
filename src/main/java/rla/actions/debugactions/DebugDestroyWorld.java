package rla.actions.debugactions;

import rla.actions.Action;
import rla.states.GameState;
import rla.states.PlayState;
import rla.tiles.TileLibrary;
import rla.world.World;

public class DebugDestroyWorld implements Action {
    @Override
    public boolean perform(GameState state) {
        ((PlayState)state).getPlayer().getMap().getTileMap().fillAll(TileLibrary.TileType.FLOOR.value);
        return false;
    }
}
