package rla.actions.debugactions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.actions.Action;
import rla.entities.items.Item;
import rla.entities.items.ItemLibrary;
import rla.entities.items.Items;
import rla.entities.items.components.FoodComponent;
import rla.entities.items.components.PotionComponent;
import rla.entities.spells.effects.HealHPSpellEffect;
import rla.states.GameState;
import rla.states.InventoryDropState;
import rla.states.PlayState;

public class DebugTestInventory implements Action {
    private GameState transitionState = null;
    @Override
    public boolean perform(GameState state) {
        return false;
    }

    @Override
    public boolean perform(GameState state, KeyStroke key) {
        transitionState = null;
        if(key.inputType() == InputType.F1) {
            transitionState = new InventoryDropState(((PlayState)state).getPlayer(), ((PlayState)state).getPlayer().getInventory(), state, ((PlayState)state).getPlayScreen());
        }
        else if(key.inputType() == InputType.F2) {
            Item biscuit = new Item('b', "biscuit", new int[]{200, 0, 0});
            biscuit.registerComponent(new FoodComponent(5000));
            Item healthPotion = new Item('p', "red potion", new int[]{200, 0, 0});
            healthPotion.registerComponent(new PotionComponent(new HealHPSpellEffect(5)));
            Items.addItem(healthPotion, ((PlayState)state).getPlayer());
            Items.addItem(biscuit, ((PlayState)state).getPlayer());
        }
        return false;
    }

    @Override
    public boolean isMultiStep() {
        return true;
    }

    @Override
    public GameState getTransitionState() {
        return transitionState;
    }


}
