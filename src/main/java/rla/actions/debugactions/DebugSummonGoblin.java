package rla.actions.debugactions;

import rla.actions.Action;
import rla.entities.actors.Actor;
import rla.entities.actors.ActorFactory;
import rla.states.GameState;
import rla.states.PlayState;

public class DebugSummonGoblin implements Action {

    @Override
    public boolean perform(GameState state) {
        Actor player = ((PlayState)state).getPlayer();
        player.getMap().placeActor(player.getMap().getActorFactory().newGoblin(), player.getX(), player.getY()-1);
        return false;
    }
}
