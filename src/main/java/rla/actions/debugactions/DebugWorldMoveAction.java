package rla.actions.debugactions;

import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import rla.actions.Action;
import rla.states.GameState;
import rla.states.PlayState;
import rla.world.World;

public class DebugWorldMoveAction implements Action {
    @Override
    public boolean perform(GameState state) {
        return false;
    }

    @Override
    public boolean perform(GameState state, KeyStroke key) {
        InputType input = key.inputType();
        World world = ((PlayState)state).getWorld();
        if(input == InputType.F1) {
            world.moveFloor(world.getCurrFloor()+1);
            return true;
        }
        else if(input == InputType.F2) {
            world.moveFloor(world.getCurrFloor()-1);
            return true;
        }
        return false;
    }

    @Override
    public boolean isMultiStep() {
        return true;
    }
}
