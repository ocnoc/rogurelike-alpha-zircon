package rla.world.algorithms.terraingenerators;

import rla.world.Map;

public interface TerrainGenerator {
    Map generateTerrain(Map map);
}
