package rla.world.algorithms.terraingenerators.tyrantgen;

import rla.tiles.TileLibrary;
import rla.tiles.TileMap;

import static rla.utilities.Utilities.generateRandomNumber;

public class RoomFeature implements Feature{
    private final int MAX_ROOM_WIDTH = 15;
    private final int MAX_ROOM_HEIGHT = 15;
    private final int MIN_ROOM_WIDTH = 4;
    private final int MIN_ROOM_HEIGHT = 4;
    int width;
    int height;
    int[] directions;
    TileMap tiles;
    int startx;
    int starty;


    @Override
    public void dig(int[] direction, int startx, int starty, TileMap tiles) {
        initRoomParams(direction, startx, starty, tiles);
        if(direction[0] == 0) {
            digVertical();
        }
        else {
            digHorizontal();
        }
    }

    private void digVertical() {
        int x1,y1,x2,y2;
        x1 = startx - width / 2;
        x2 = startx + width / 2;
        y1 = starty;
        y2 = starty + (directions[1] * height); // the multiplier decides the direction. if d[1] is pos, we go south. if neg, we go north
        buildRoom(x1, y1, x2, y2);
    }

    private void digHorizontal() {
        int x1,y1,x2,y2;
        x1 = startx;
        x2 = startx + (directions[0] * width);
        y1 = starty - (height / 2);
        y2 = starty + (height / 2) ; // the multiplier decides the direction. if d[1] is pos, we go south. if neg, we go north
        buildRoom(x1, y1, x2, y2);
    }

    // For original room gen (each room connects to a given neighbor once), switch back to swapping large/small 1/2 values BEFORE adding, and remove the adds on impassable check
    private void buildRoom(int x1, int y1, int x2, int y2) {
        final int CHECK_PADDING = 0;
        final int DIG_PADDING = 1;
        if(!(tiles.areaImpassable(x1, y1, x2, y2, CHECK_PADDING))) {
            return;
        }
        else {
            tiles.paintRect(x1, y1, x2, y2, TileLibrary.TileType.FLOOR.value, DIG_PADDING);
            tiles.paint(startx, starty, TileLibrary.getTile(TileLibrary.TileType.DOOR));
        }
    }


    private void initRoomParams(int[] direction, int startx, int starty, TileMap tiles) {
        this.directions = direction;
        this.startx = startx;
        this.starty = starty;
        this.tiles = tiles;
        width = generateRandomNumber(MIN_ROOM_WIDTH, MAX_ROOM_WIDTH);
        height = generateRandomNumber(MIN_ROOM_HEIGHT, MAX_ROOM_HEIGHT);
    }

}
