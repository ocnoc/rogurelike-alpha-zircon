package rla.world.algorithms.terraingenerators.tyrantgen;

import rla.utilities.Utilities;
import rla.tiles.TileLibrary;
import rla.tiles.TileMap;

public class CorridorFeature implements Feature {
    private final int MAX_LENGTH = 9;
    private final int MIN_LENGTH = 9;
    private final double FLOOR_CHANCE = .9;
    int[] directions;
    TileMap tiles;
    int startx;
    int starty;
    int length;

    @Override
    public void dig(int[] direction, int startx, int starty, TileMap tiles) {
        initParams(direction, startx, starty, tiles);
        if(direction[0] == 0) {
            digVertical();
        }
        else {
            digHorizontal();
        }
    }

    private void digHorizontal() {
        int x1 = startx;
        int x2 = startx + (directions[0] * length);
        int y1 = starty;
        int y2 = starty ;
        buildCorridor(x1, y1, x2, y2);
    }

    private void digVertical() {
        int x1 = startx;
        int x2 = startx  ;
        int y1 = starty;
        int y2 = starty + (directions[1] * length);
        buildCorridor(x1, y1, x2, y2);
    }

    private void buildCorridor(int x1, int y1, int x2, int y2) {
        // custom padding on these, we want to check an extra tile around our tunnel to ensure we dont tunnel into a room- we just want to build rooms.
        if(!(tiles.areaImpassable(x1-1, y1-1, x2+1, y2+1, 0))) {
            return;
        }
        else {
            tiles.paintRect(x1, y1, x2, y2, TileLibrary.TileType.FLOOR.value, 0);
            tiles.paint(startx, starty, Math.random() < FLOOR_CHANCE ? TileLibrary.getTile(TileLibrary.TileType.FLOOR) : TileLibrary.getTile(TileLibrary.TileType.DOOR));
        }
    }

    private void initParams(int[] direction, int startx, int starty, TileMap tiles) {
        this.directions = direction;
        this.startx = startx;
        this.starty = starty;
        this.tiles = tiles;
        length = Utilities.generateRandomNumber(MIN_LENGTH, MAX_LENGTH);
    }
}
