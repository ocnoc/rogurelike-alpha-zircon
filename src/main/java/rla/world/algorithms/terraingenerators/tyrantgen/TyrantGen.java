package rla.world.algorithms.terraingenerators.tyrantgen;

import rla.tiles.TileLibrary;
import rla.tiles.TileMap;
import rla.world.Map;
import rla.world.algorithms.terraingenerators.TerrainGenerator;

import static rla.utilities.Utilities.generateRandomNumber;

/**
 * Implementation of dungeon generating algroithm found in Tyrant game described by Mike Anderson
 * http://www.roguebasin.com/index.php?title=Dungeon-Building_Algorithm
 */
public class TyrantGen implements TerrainGenerator {
    private final int MAX_ROOM_WIDTH = 7;
    private final int MAX_ROOM_HEIGHT = 7;
    private final int MIN_ROOM_WIDTH = 3;
    private final int MIN_ROOM_HEIGHT = 3;

    private TileMap tiles;
    private Map map;
    private int width;
    private int height;
    private int iterations;

    public TyrantGen(int iterations) {
        this.iterations = iterations;
    }


    /**
     * Generate function, calls the function that begins building a map
     * @return a new map
     */
    @Override
    public Map generateTerrain(Map map) {
        this.map = map;
        this.tiles = map.getTileMap();
        this.width = tiles.getWidth();
        this.height = tiles.getHeight();
        tyrantStart();
        return map;
    }

    /**
     * Initialzies a filled room and works from there
     */
    private void tyrantStart() {
        tiles.fillAll(TileLibrary.TileType.WALL.value);
        digCenterRoom();
        makeFeatures();
    }

    /*
    TODO: Have this grab from a random list of actors
     */

    /**
     * Picks a feature based on a certain probability, then attempts to implement it
     * @return
     */
    public Feature chooseFeature() {
        //return new RoomFeature();
        return Math.random() < .8 ? new RoomFeature() : new CorridorFeature();
    }

    private void makeFeatures() {
        for(int i=0;i<iterations;i++) {
            boolean foundWall = false;
            int wallX = 0;
            int wallY = 0;
            int[] directions = new int[2];

            while (!foundWall) {
                wallX = generateRandomNumber(0, width-1);
                wallY = generateRandomNumber(0, height-1);
                directions = tiles.checkAir(wallX, wallY);
                foundWall = directions != null;
            }
            chooseFeature().dig(directions, wallX, wallY, tiles);
        }
    }

    private void digCenterRoom() {
        int centerx = width / 2; // find center x
        int centery = height / 2; // find center y
        int width = generateRandomNumber(MIN_ROOM_WIDTH, MAX_ROOM_WIDTH); // make a random width
        int height = generateRandomNumber(MIN_ROOM_HEIGHT, MAX_ROOM_HEIGHT); // make a random height

        int x1 = centerx - width / 2;
        int x2 = centerx + width / 2;
        int y1 = centery - height / 2;
        int y2 = centery + height / 2;

        tiles.paintRect(x1, y1, x2, y2, TileLibrary.TileType.FLOOR.value, 0);
    }
}
