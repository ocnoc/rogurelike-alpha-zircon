package rla.world.algorithms.terraingenerators.tyrantgen;

import rla.tiles.TileMap;

public interface Feature {
    void dig(int[] direction, int startx, int starty, TileMap tiles);
}
