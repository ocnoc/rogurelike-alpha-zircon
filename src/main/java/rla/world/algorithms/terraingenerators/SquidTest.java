package rla.world.algorithms.terraingenerators;

import rla.tiles.Tile;
import rla.tiles.TileLibrary;
import rla.tiles.TileMap;
import rla.world.Map;
import squidpony.squidgrid.mapping.DungeonGenerator;
import squidpony.squidgrid.mapping.styled.TilesetType;

public class SquidTest implements TerrainGenerator{
    private Map map;
    private TileMap tiles;
    char[][] dungeonMap;

    private int width;
    private int height;

    public SquidTest(Map map) {
        this.map = map;
        this.width = tiles.getWidth();
        this.height = tiles.getHeight();
    }

    @Override
    public Map generateTerrain(Map map) {
        generateDungeonMap();
        parseMap();
        return map;
    }

    private void parseMap() {
        for(int x=0;x<width;x++) {
            for(int y=0;y<height;y++) {
                tiles.paint(x, y, parseChar(dungeonMap[x][y]));
            }
        }
    }

    private Tile parseChar(char c) {
        switch (c) {
            case '#':
                return TileLibrary.getWall();
            default:
                return TileLibrary.getFloor();
        }
    }

    private void generateDungeonMap() {
        DungeonGenerator dungeonGenerator = new DungeonGenerator(width, height);
        dungeonMap = Math.random() < .5 ? dungeonGenerator.generate(TilesetType.OPEN_AREAS) : dungeonGenerator.generate(TilesetType.ROOMS_AND_CORRIDORS_A);
    }
}
