package rla.world.algorithms.actorplacementgenerators;

import rla.entities.actors.Actor;
import rla.entities.actors.ActorFactory;
import rla.world.Map;

public class RandomActorPlacer implements ActorPlacementGenerator{
    private final int MAX_ACTORS;

    public RandomActorPlacer(int MAX_ACTORS) {
        this.MAX_ACTORS = MAX_ACTORS;
    }

    @Override
    public Map placeActors(Map map) {
        ActorFactory afactory = map.getActorFactory();
        for(int i=0;i<MAX_ACTORS;i++) {
            Actor actor = afactory.getRandomActor();
            map.addActorAtEmptyLocation(actor);
        }
        return map;
    }
}
