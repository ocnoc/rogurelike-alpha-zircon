package rla.world.algorithms.actorplacementgenerators;

import rla.world.Map;

public interface ActorPlacementGenerator {
    Map placeActors(Map map);
}
