package rla.world.algorithms.itemplacementgenerators;

import rla.world.Map;

public interface ItemPlacementGenerator {
    Map placeItems(Map map);
}
