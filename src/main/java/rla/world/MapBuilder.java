package rla.world;

import rla.entities.actors.ActorFactory;
import rla.entities.actors.ActorMap;
import rla.entities.timing.TimeSentinel;
import rla.tiles.TileMap;
import rla.world.algorithms.actorplacementgenerators.ActorPlacementGenerator;
import rla.world.algorithms.itemplacementgenerators.ItemPlacementGenerator;
import rla.world.algorithms.terraingenerators.TerrainGenerator;

public class MapBuilder {
    // TODO: Split into TerrainGenerators, ActorPlacementGenerators and ItemPlacementGenerators
    private Map map;
    private TerrainGenerator tgen;
    private ActorPlacementGenerator agen;
    private ItemPlacementGenerator igen;
    private int width, height;

    public MapBuilder(TerrainGenerator tgen, ActorPlacementGenerator agen, ItemPlacementGenerator igen, int width, int height) {
        this.tgen = tgen;
        this.igen = igen;
        this.agen = agen;
        this.width = width;
        this.height = height;
    }

    public Map build() {
        map = tgen.generateTerrain(new Map(new TileMap(width, height), new ActorMap(), new ActorFactory(TimeSentinel.getTimeSentinel())));
        if(tgen == null) {
            System.out.println("Error building map! Terrain generator failed.");
            System.exit(0);
        }
        tgen.generateTerrain(map);
        if(agen != null) {
            agen.placeActors(map);
        }
        if(igen != null) {
            igen.placeItems(map);
        }
        return map;
    }

}
