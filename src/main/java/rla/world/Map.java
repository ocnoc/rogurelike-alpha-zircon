package rla.world;

import rla.entities.actors.Actor;
import rla.entities.actors.ActorFactory;
import rla.entities.actors.ActorMap;
import rla.entities.items.ItemLibrary;
import rla.entities.items.Items;
import rla.entities.timing.TimeSentinel;
import rla.tiles.Tile;
import rla.tiles.TileLibrary;
import rla.tiles.TileMap;

import java.util.LinkedList;
import java.util.List;

public class Map {
    private TileMap tiles;
    private ActorMap actors;
    private int width;
    private int height;
    private int stairdx = -1, stairdy = -1, stairux = -1, stairuy = -1;
    ActorFactory actorFactory;

    public Map(TileMap tiles, ActorMap actors, ActorFactory actorFactory) {
        this.tiles = tiles;
        this.actors = actors;
        this.width = tiles.getWidth();
        this.height = tiles.getHeight();
        this.actorFactory = actorFactory;
        actorFactory.setMap(this);
    }

    public Tile getTile(int x, int y) {
        if (x < 0 | x >= width | y < 0 | y >= height) {
            return TileLibrary.getTile(2);
        }
        else {
            return tiles.getTile(x, y);
        }
    }

    public TileMap getTileMap() {
        return tiles;
    }

    private void registerActor(Actor a) {
        if(!(actors.getActorList().contains(a))) {
            actors.addActor(a);
        }
    }

    public Actor actorAt(int x, int y) {
        return actors.getActorAt(x, y);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char[][] getVisionMap() {
        return tiles.getVisionMap();
    }

    public char[][] getPassabilityMap() {
        return tiles.getPassabilityMap();
    }

    public int[] findStairs(boolean down) {
        return down ? new int[] {stairdx, stairdy} : new int[] {stairux, stairuy};
    }

    public void randomlyPlaceStairs(boolean down) {
        int[] coords = findRandomPassableCoordinates();
        placeStairs(down, coords[0], coords[1]);
    }

    /**
     * Puts either down or up stairs at specified coordinates
     * @param down down stairs
     */
    public void placeStairs(boolean down, int x, int y) {
        ItemLibrary.ItemTemplates stairType = ItemLibrary.ItemTemplates.STAIRS_DOWN;
        if(!down) {
            stairType = ItemLibrary.ItemTemplates.STAIRS_UP;
        }
        Items.addItem(ItemLibrary.getItem(stairType.itemid), getTile(x, y));
        if(down) {
            stairdx = x;
            stairdy = y;
        }
        else {
            stairux = x;
            stairuy = y;
        }
    }

    public void addActorAtEmptyLocation(Actor actor) {
        int x;
        int y;

        do {
            x = (int)(Math.random() * width);
            y = (int)(Math.random() * height);
        } while(!(getTile(x, y).isPassable()) || actors.getActorAt(x, y) != null);

        placeActor(actor, x, y);
    }

    public ActorMap getActors() {
        return actors;
    }

    /**
     * Calls all update methods, use only this one in general
     */
    public void update() {
        List<Actor> alist = new LinkedList<>(actors.getActorList());
        for(Actor actor : alist) {
            actor.update();
        }

    }

    public Actor getActor(int x, int y) {
        return actors.getActorAt(x, y);
    }

    public void removeActor(Actor actor) {
        actors.removeActor(actor);
    }

    /**
     * Put a given actor on a specific x/y coordinate. Does not care about overlapping actors.
     * @param actor the actor to place
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public void placeActor(Actor actor, int x, int y) {
        actor.setX(x);
        actor.setY(y);
        registerActor(actor);
    }

    /**
     * Gives coordiantes to a random tile that's passable
     * @return coordiante pair x, y as an array
     */
    public int[] findRandomPassableCoordinates() {
        int x;
        int y;

        do {
            x = (int)(Math.random() * width);
            y = (int)(Math.random() * height);
        } while(!(getTile(x, y).isPassable()));
        return new int[] {x, y};
    }

    public ActorFactory getActorFactory() {
        return actorFactory;
    }
}
