package rla.world;

import rla.entities.actors.Actor;
import rla.entities.actors.ActorFactory;
import rla.entities.timing.TimeSentinel;
import rla.world.algorithms.actorplacementgenerators.RandomActorPlacer;
import rla.world.algorithms.terraingenerators.tyrantgen.TyrantGen;

import java.util.ArrayList;
import java.util.List;

public class World {
    private List<Map> floors;
    private Map currentMap;
    private int currFloor;
    private Actor player;
    private TimeSentinel sentinel;
    private final int DEFAULT_MAP_X = 20;
    private final int DEFAULT_MAP_Y = 20;
    private final int WORLD_GEN_ITERATIONS = 300; // iterations of whatever algorithm needs it

    public World(TimeSentinel sentinel) {
        this.sentinel = sentinel;
        floors = new ArrayList<>();
        currentMap = createFloor(0);
        currFloor = 0;
        initializePlayer();
    }

    /**
     * Checks if floor 'floor' exists. If not, returns false.
     * @param floor floor number
     * @return if there exists a map at floor 'floor'
     */
    public boolean checkFloorExists(int floor) {
        try {
            return floors.get(floor) != null;
        }
        catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public void initializePlayer() {
        player = currentMap.getActorFactory().newPlayer();
        sentinel.registerPlayer(player);
        currentMap.addActorAtEmptyLocation(player);
    }


    public void update() {
        currentMap.update();
    }

    /**
     * Returns the floor at i. Does not check for null.
     * @param i floor number
     * @return floor at i or null
     */
    public Map getFloor(int i) {
        return floors.get(i);
    }

    /**
     * Creates a new map with default parameters and places it at index i of floors.
     * @param i the floor number to create
     * @return the map created
     */
    public Map createFloor(int i) {
        Map m = createMap(DEFAULT_MAP_X, DEFAULT_MAP_Y, WORLD_GEN_ITERATIONS);
        m.randomlyPlaceStairs(true);
        floors.add(i, m);
        return m;
    }

    /**
     * Creates a new map given parameters
     * @param x map width
     * @param y map height
     * @param iterations iterations of creation algorithm
     * @return map created
     */
    public Map createMap(int x, int y, int iterations) {
        ActorFactory actorFactory = new ActorFactory(sentinel);
        return new MapBuilder(new TyrantGen(iterations), new RandomActorPlacer(15), null, x, y).build();
    }

    /**
     * Creates a new map for indicated floor if it doesn't exist. Then sets the floor to be the current map, setss the player to exist on that floor, and if it's intersecting an invalid position, moves it randomly.
     * @param floor floor number to move to
     */
    public void moveFloor(int floor) {
        if(floor < 0) {
            return;
        }
        if(!(checkFloorExists(floor))) {
            createFloor(floor);
        }
        currentMap.getActors().getActorList().forEach(a -> sentinel.release(a));
        currentMap = floors.get(floor);
        currentMap.getActors().getActorList().forEach(a -> sentinel.register(a));
        movePlayerToFloor(floor, floor < currFloor);
        this.currFloor = floor;
    }

    /**
     * Handles moving the player to specified floor i
     * @param i floor number
     */
    private void movePlayerToFloor(int i, boolean goingUp) {
        player.setMap(getFloor(i));
        if(goingUp) {
            int[] coords = floors.get(i).findStairs(true);
            getFloor(i).placeActor(player, coords[0], coords[1]);
        }
        else {
            int[] coords = floors.get(i).findStairs(false);
            if(coords[0] == -1 || coords[1] == -1) {
                getFloor(i).randomlyPlaceStairs(false);
                coords = floors.get(i).findStairs(false);
            }
            getFloor(i).placeActor(player, coords[0], coords[1]);
        }
    }

    // GENERIC GETTERS AND SETTERS BELOW
    public List<Map> getFloors() {
        return floors;
    }

    public void setFloors(List<Map> floors) {
        this.floors = floors;
    }

    public Map getCurrentMap() {
        return currentMap;
    }

    public void setCurrentMap(Map currentMap) {
        this.currentMap = currentMap;
    }

    public int getCurrFloor() {
        return currFloor;
    }

    public void setCurrFloor(int currFloor) {
        this.currFloor = currFloor;
    }

    public Actor getPlayer() {
        return player;
    }

    public void setPlayer(Actor player) {
        this.player = player;
    }
}
