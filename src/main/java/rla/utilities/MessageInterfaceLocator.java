package rla.utilities;

public class MessageInterfaceLocator {
    private static MessageInterface logger;

    public static MessageInterface getMessageLog() {
        return logger;
    }

    public static void provide(MessageInterface messageLog) {
        logger = messageLog;
    }
}
