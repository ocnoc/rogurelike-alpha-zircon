package rla.utilities;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageLog implements MessageInterface {
    private Queue<String> messages;
    private MessageDisplayer display;

    public MessageLog() {

        this.messages = new ConcurrentLinkedQueue<>();
    }

    public void sendMessage(String message, Object ... params) {
        messages.add(String.format(message, params));
    }

    public void sendMessage(String msg) {
        messages.add(msg);
        if(display != null) {
            display.displayMessages();
        }
    }

    public void setDisplay(MessageDisplayer display) {
        this.display = display;
    }

    /**
     * Removes the message returned
     * @return oldest message in the queue
     */
    public String popOldestMessage() {
        return messages.poll();
    }

    public boolean hasNext() {
        return messages.peek() != null;
    }
}
