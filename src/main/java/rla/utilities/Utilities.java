package rla.utilities;

import squidpony.squidmath.Dice;

import java.util.Random;

public class Utilities {

    public static int generateRandomNumber(int lower, int upper) {
        Random random = new Random();
        upper += 1;
        return random.nextInt(upper-lower) + lower;
    }

    public static String makeSecondPerson(String msg) {
        String[] split = msg.split(" ");
        split[0] = split[0] + "s";
        StringBuilder builder = new StringBuilder();
        for(String s : split) {
            builder.append(" ");
            builder.append(s);
        }
        return builder.toString().trim();
    }

    public static String generateDiceString(int dice, int sides) {
        StringBuilder r = new StringBuilder();
        r.append(dice);
        r.append("d");
        r.append(sides);
        return r.toString();
    }

}
