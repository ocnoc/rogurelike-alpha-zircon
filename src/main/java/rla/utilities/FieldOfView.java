package rla.utilities;

import rla.tiles.Tile;
import rla.tiles.TileLibrary;
import rla.tiles.TileMap;
import squidpony.squidgrid.FOV;
import squidpony.squidgrid.mapping.DungeonUtility;

import java.util.HashMap;
import java.util.Map;

public class FieldOfView {
    double[][] fovMap;
    private FOV fovCalculator;
    private Map<TileMap, TileMap> memoryMapping;

    public double[][] updateMemory(TileMap tmap, int startx, int starty, int vr) {
        double[][] resistances = DungeonUtility.generateResistances(tmap.getVisionMap());
        double[][] fov = fovCalculator.calculateFOV(resistances, startx, starty, vr);
        TileMap memory = getMapMemory(tmap);

        for(int x=0;x<fov.length;x++) {
            for(int y=0;y<fov[0].length;y++) {
                if(fov[x][y] > 0) {
                    memory.paint(x, y, tmap.getTile(x, y));
                }
            }
        }
        fovMap = fov;
        return fov;
    }

    public boolean canSee(double[][] resistances, int startx, int starty, int targetX, int targetY, int visionRadius) {
        double[][] vision = fovCalculator.calculateFOV(resistances, startx, starty, visionRadius);
        return vision[targetX][targetY] > 0;
    }

    public FieldOfView() {
        fovCalculator = new FOV();
        memoryMapping = new HashMap<>();
    }

    private TileMap getMapMemory(TileMap tmap) {
        if(memoryMapping.get(tmap) == null) {
            createMapMemory(tmap);
        }
        return memoryMapping.get(tmap);
    }

    public Tile getMemory(int x, int y, TileMap tmap) {
        return getMapMemory(tmap).getTile(x, y);
    }

    public double getVisibility(int x, int y) {
        try {
            return fovMap[x][y];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    private void createMapMemory(TileMap tmap) {
        TileMap memory = new TileMap(tmap.getWidth(), tmap.getHeight());
        memory.fillAll(TileLibrary.TileType.OOB.value);
        memoryMapping.put(tmap, memory);
    }
}
