package rla.utilities;

public interface MessageInterface {
    void sendMessage(String message);
    String popOldestMessage();
    boolean hasNext();
    void setDisplay(MessageDisplayer display);
}
