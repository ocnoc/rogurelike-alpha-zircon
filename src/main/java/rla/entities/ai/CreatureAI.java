package rla.entities.ai;

import rla.entities.actors.Actor;
import rla.tiles.Tile;
import rla.utilities.FieldOfView;

public abstract class CreatureAI {
    public abstract boolean onEnter(int x, int y, Tile tile);

    public abstract int onUpdate();

    public abstract boolean onAttack(Actor target);

    public abstract boolean canSee(char[][] visibilityMap, int wx, int wy);

    public abstract double[][] updateFOV();

    public void onNotify(String msg) {
    }

    public FieldOfView getFov() {
        return null;
    }
}
