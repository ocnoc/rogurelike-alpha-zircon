package rla.entities.ai;

import rla.utilities.Utilities;
import rla.entities.actors.Actor;
import rla.entities.actors.ActorFactory;
import rla.tiles.Tile;

/**
 * AI that never decides to move.
 */
public class FungusAI extends GenericAI {
    private final double CHANCE_TO_SPREAD = .02;

    public FungusAI(Actor actor) {
        super(actor);
    }

    @Override
    public boolean onEnter(int x, int y, Tile tile) {
        return false;
    }

    @Override
    public int onUpdate() {
        if(Math.random() < CHANCE_TO_SPREAD) {
            spread();
        }
        return 100;
    }

    @Override
    public double[][] updateFOV() {
        return new double[0][];
    }

    private void spread() {
        final String msg = "spread";
        int x = actor.getX() + Utilities.generateRandomNumber(-1, 1);
        int y = actor.getY() + Utilities.generateRandomNumber(-1, 1);

        if(!(actor.canEnter(x, y))) {
            return;
        }
        actor.doAction(msg);

        ActorFactory actorFactory = actor.getMap().getActorFactory();
        Actor child = actorFactory.newFungus();
        actorFactory.insertActor(child);
        child.setX(x);
        child.setY(y);
    }


}
