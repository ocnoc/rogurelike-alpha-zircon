package rla.entities.ai;

import rla.utilities.Utilities;
import rla.entities.actors.Actor;
import rla.tiles.Tile;
import squidpony.squidgrid.LOS;

public class GenericAI extends CreatureAI{
    Actor actor;

    public GenericAI(Actor actor) {
        this.actor = actor;
        actor.setCreatureAI(this);
    }

    @Override
    public boolean onEnter(int x, int y, Tile tile) {
        if(tile.isPassable()) {
            actor.setX(x);
            actor.setY(y);
            return true;
        }
        else {
            return tile.onBump(actor);
        }
    }

    public void wander() {
        int mx = Utilities.generateRandomNumber(-1, 1);
        int my = Utilities.generateRandomNumber(-1, 1);
        actor.moveBy(mx, my);
    }

    @Override
    public int onUpdate() {
        wander();
        return 25;
    }

    @Override
    public boolean onAttack(Actor target) {
        int damage = Math.max(0, actor.getAtk() - target.getDef());
        damage = Utilities.generateRandomNumber(0, damage);
        target.modifyHp(-damage);
        return true;
    }

    @Override
    public boolean canSee(char[][] visibilityMap, int wx, int wy) {
        int distX = actor.getX() - wx;
        int distY = actor.getY() - wy;
        int vr = actor.getVisionRadius();
        LOS los = new LOS();
        if(distX*distX + distY*distY > vr*vr ) {
            return false;
        }
        return los.isReachable(visibilityMap, actor.getX(), actor.getY(), wx, wy);
    }

    @Override
    public double[][] updateFOV() {
        return new double[0][];
    }
}
