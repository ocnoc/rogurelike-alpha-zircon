package rla.entities.ai;

import rla.utilities.FieldOfView;
import rla.utilities.MessageInterfaceLocator;
import rla.utilities.MessageLog;
import rla.utilities.Utilities;
import rla.entities.actors.Actor;
import rla.tiles.Tile;
import squidpony.squidgrid.LOS;
import squidpony.squidgrid.mapping.DungeonUtility;

public class PlayerAI extends CreatureAI {
    Actor actor;
    FieldOfView fov;

    public PlayerAI(Actor actor) {
        this.actor = actor;
        actor.setCreatureAI(this);
        fov = new FieldOfView();
    }

    @Override
    public boolean onEnter(int x, int y, Tile tile) {
        if(tile.isPassable()) {
            actor.setX(x);
            actor.setY(y);
            if(!(tile.getInventory().isEmpty())) {
                StringBuilder itemlist = new StringBuilder();
                tile.getInventory().getItemList().forEach(i -> itemlist.append("" + i.getName() + ", "));
                itemlist.delete(itemlist.length()-2, itemlist.length());
                actor.notify("You see %s on the ground", itemlist.toString());
            }
            return true;
        }
        else {
            return tile.onBump(actor);
        }
    }

    @Override
    public int onUpdate() {
        return 0;
    }

    @Override
    public boolean onAttack(Actor target) {
        int damage = Math.max(0, actor.getAtk() - target.getDef());
        damage = Utilities.generateRandomNumber(0, damage);
        target.modifyHp(-damage);
        return true;
    }

    @Override
    public boolean canSee(char[][] visibilityMap, int wx, int wy) {
        return fov.canSee(DungeonUtility.generateResistances(visibilityMap), actor.getX(), actor.getY(), wx, wy, actor.getVisionRadius());
        /*
        int distX = actor.getX() - wx;
        int distY = actor.getY() - wy;
        int vr = actor.getVisionRadius();
        LOS los = new LOS();
        if(distX*distX + distY*distY > vr*vr ) {
            return false;
        }
        return los.isReachable(visibilityMap, actor.getX(), actor.getY(), wx, wy);
        */
    }

    @Override
    public void onNotify(String msg) {
        MessageInterfaceLocator.getMessageLog().sendMessage(msg);
    }

    public double[][] updateFOV() {
        return fov.updateMemory(actor.getMap().getTileMap(), actor.getX(), actor.getY(), actor.getVisionRadius());
    }

    public FieldOfView getFov() {
        return fov;
    }
}
