package rla.entities.actors;

import rla.entities.Entity;
import rla.entities.ItemContainer;
import rla.entities.RLAComponent;
import rla.entities.items.Items;
import rla.entities.items.components.FoodComponent;
import rla.entities.timing.TimeBasedEntity;
import rla.entities.ai.CreatureAI;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.tiles.Tile;
import rla.utilities.FieldOfView;
import rla.utilities.Utilities;
import rla.world.Map;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Actor implements Cloneable, ItemContainer, TimeBasedEntity, Entity {
    // Time Sentinel
    private int actionPoints;

    // Location
    private Map map; // the map this actor exists in
    private int x;
    private int y;
    private int visionRadius;

    // Stats
    private int maxhp;
    private int hp;
    private int atk;
    private int def;
    private boolean dead;

    // Rendering
    private char glyph;
    private int[] color;
    private String name;

    //Components
    private Inventory inventory;
    private CreatureAI ai;
    private List<RLAComponent> components = new ArrayList<>();

    public Actor(Map map, String name, char glyph, int[] color, int maxhp, int atk, int def, Class<? extends CreatureAI> defaultAI, int visionRadius) {
        this.name = name;
        this.maxhp = maxhp;
        this.hp = maxhp;
        this.atk = atk;
        this.def = def;
        this.map = map;
        this.glyph = glyph;
        this.color = color;
        this.visionRadius = visionRadius;
        dead = false;
        this.inventory = new Inventory();
        actionPoints = 0;
        if(CreatureAI.class.isAssignableFrom(defaultAI) ) {
            try {
                ai = defaultAI.asSubclass(CreatureAI.class).getConstructor(Actor.class).newInstance(this);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    public void registerComponent(RLAComponent component) {
        if(getComponent(component.getClass()) != null) {
            return;
        }
        components.add(component);
    }

    public RLAComponent getComponent(Class<? extends RLAComponent> componentType) {
        for (RLAComponent c : components) {
            if (c.getClass() == componentType) {
                return c;
            }
        }
        return null;
    }

    public Inventory getInventory() {
        return inventory;
    }

    //TODO convert this to using Items.blah
    public boolean getItem(Item item) {
        if(Items.moveItem(item.getParent(), item, this)) {
            doAction("get %s", item.getName());
            return true;
        }
        doAction("can't carry that");
        return false;
    }

    public boolean drop(Item item) {
        if(map.getTile(x, y).getInventory().isFull()) {
            doAction("can't find any room");
            return false;
        }
        Items.moveItem(item.getParent(), item, map.getTile(x, y));
        doAction("drop the %s", item.getName());
        return true;
    }

    public void setCreatureAI(CreatureAI ai) {
        this.ai = ai;
    }

    public boolean moveBy(int mx, int my) {
        if(mx == 0 && my == 0) {
            return false;
        }
        Actor target = map.getActor(x+mx, y+my);

        if(target == null) {
            return ai.onEnter(x + mx, y + my,
                    map.getTile(x+mx, y+my));
        }
        else {
            return attack(target);
        }
    }

    public Tile getCurrentTile() {
        return map.getTile(x, y);
    }

    public void doAction(String msg, Object ... params) {
        map.getActors().getActorList().forEach(a -> {
            if(a.canSee(map.getVisionMap(), x, y)) {
                if(a == this) {
                    a.notify("You " + msg + "", params);
                }
                else {
                    a.notify(String.format("The %s %s", name, Utilities.makeSecondPerson(msg)), params);
                }
            }
        });
    }

    public boolean attack(Actor target) {
        final String message = "attack %s";
        doAction(message, target.name);
        return ai.onAttack(target);
    }

    public boolean canEnter(int x, int y) {
        return map.getTile(x, y).isPassable() && map.getActor(x, y) == null;
    }

    public int update() {
        for(RLAComponent component : components) {
            component.update();
        }
        return ai.onUpdate();
    }

    @Override
    public int getSpeed() {
        return 100;
    }

    public void modifyHp(int mod) {
        hp += mod;
        if(hp <= 0) {
            die();
        }
    }

    public void notify(String msg, Object ... params) {
        ai.onNotify(String.format(msg, params));
    }

    public void changeMap(Map target) {
        if(target != null) {
            map = target;
        }
    }

    public boolean dig(int x, int y) {
        if(map.getTile(getX()+x, getY()+y).isDiggable()) {
            return map.getTile(getX()+x, getY()+y).onDig(this);
        }
        return false;
    }

    public void die() {
        doAction("die");
        leaveCorpse();
        dead = true;
        map.removeActor(this);
    }

    public void leaveCorpse() {
        Item corpse = new Item('%', "corpse of " + name, color);
        corpse.registerComponent(new FoodComponent(maxhp * 10));
        Items.addItem(corpse, map.getTile(x, y));
    }

    public void modHp(int mod) {
        hp += mod;
        if(hp <= 0) {
            die();
        }
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getGlyph() {
        return glyph;
    }

    public void setGlyph(char glyph) {
        this.glyph = glyph;
    }

    public int[] getColor() {
        return color;
    }

    public void setColor(int[] color) {
        this.color = color;
    }

    public int getMaxhp() {
        return maxhp;
    }

    public void setMaxhp(int maxhp) {
        this.maxhp = maxhp;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public double[][] updateFOV() {
        return ai.updateFOV();
    }

    public boolean canSee(char[][] visibilityMap, int wx, int wy) {
        return ai.canSee(visibilityMap, wx, wy);
    }

    public int getVisionRadius() {
        return visionRadius;
    }

    public void setVisionRadius(int visionRadius) {
        this.visionRadius = visionRadius;
    }

    public Object clone() {
        try {
            Actor clone  = (Actor)super.clone();
            clone.ai.getClass().getConstructor(Actor.class).newInstance(clone);
            return clone;
        } catch (CloneNotSupportedException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void addActionPoints(int i) {
        actionPoints += i;
    }

    @Override
    public int getActionPoints() {
        return actionPoints;
    }

    @Override
    public void setActionPoints(int i) {
        actionPoints = i;
    }

    @Override
    public boolean toDeregister() {
        return dead;
    }

    public FieldOfView getFov() {
        return ai.getFov();
    }
}
