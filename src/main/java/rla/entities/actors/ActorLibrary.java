package rla.entities.actors;

import rla.datahandling.ExternalDataHub;

import java.util.List;

public class ActorLibrary {
    private static List<Actor> actors;

    public static void init() {
        actors = ExternalDataHub.
                getHub().
                getActorLoader().
                loadActorData();
    }

    public static int getNumLoadedActors() {
        return actors.size();
    }

    public static Actor getActor(int id) {
        Actor actor = (Actor) actors.get(id).clone();
        return actor;
    }
}
