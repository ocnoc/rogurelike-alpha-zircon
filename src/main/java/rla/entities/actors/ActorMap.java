package rla.entities.actors;

import java.util.LinkedList;
import java.util.List;

public class ActorMap {
    private List<Actor> actors;

    public ActorMap() {
        actors = new LinkedList<>();
    }

    public Actor getActorAt(int x, int y) {
        Actor actor;
        actor = actors.stream(). // get a stream of actors
                filter(a -> a.getX() == x && a.getY() == y). // only keeps actors that have x == x and y == y
                findFirst(). // find the first one left
                orElse(null); // or return null
        return actor;
    }

    public void addActor(Actor actor) {
        actors.add(actor);
    }

    public void removeActor(Actor actor) {
        actors.remove(actor);
    }

    public List<Actor> getActorList() {
        return actors;
    }
}
