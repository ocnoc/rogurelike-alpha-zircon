package rla.entities.actors;

import rla.entities.actors.components.BodyComponent;
import rla.entities.actors.components.HungerComponent;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.entities.actors.components.bodyparts.BodyParts;
import rla.entities.timing.TimeSentinel;
import rla.utilities.Utilities;
import rla.world.Map;

public class ActorFactory {
    private Map map;
    private TimeSentinel sentinel;

    public ActorFactory(TimeSentinel sentinel) {
        this.sentinel = sentinel;
    }

    public void setMap(Map map) {
        this.map = map;
    }
    public Map getMap() {
        return map;
    }


    public Actor newPlayer() {
        Actor player = getActor(0);
        player.registerComponent(new HungerComponent(player));
        player.registerComponent(new BodyComponent(player));
        ((BodyComponent)player.getComponent(BodyComponent.class)).addPart(new BodyPart(BodyParts.BodyPartsEnum.BODY));
        return player;
    }

    public Actor newFungus() {
        return getActor(1);
    }

    public Actor newGoblin() {
        return getActor(2);
    }

    public Actor getActor(int id) {
        Actor actor = ActorLibrary.getActor(id);
        actor.setMap(map);
        sentinel.register(actor);
        return actor;
    }

    public Actor getRandomActor() {
        int selection = Utilities.generateRandomNumber(1, ActorLibrary.getNumLoadedActors()-1);
        return getActor(selection);
    }

    public void insertActor(Actor actor) {
        map.placeActor(actor, actor.getX(), actor.getY());
    }
}
