package rla.entities.actors.components;

import rla.entities.RLAComponent;
import rla.entities.actors.Actor;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.entities.actors.components.bodyparts.BodyParts;
import rla.entities.items.Item;
import rla.entities.items.Items;

import java.util.ArrayList;
import java.util.List;

public class BodyComponent implements RLAComponent {
    Actor parent;
    List<BodyPart> bodyparts;

    final String UNEQUIP_MSG = "unequip the %s from the %s.";
    final String EQUIP_MSG = "equip the %s to the %s.";

    public BodyComponent(Actor parent) {
        this.parent = parent;
        bodyparts = new ArrayList<>();
    }

    public boolean equip(BodyPart part, Item item) {
        Items.moveItem(item.getParent(), item, part);
        parent.doAction(EQUIP_MSG, item.getName(), part.getName());
        return true;
    }

    public void unequip(BodyPart part) {
        if(part.getInventory().isEmpty()) {
            return;
        }
        Item item = part.getInventory().getItem(0);
        Items.moveItem(part, item, parent);
        parent.doAction(UNEQUIP_MSG, item.getName(), part.getName());

    }

    public List<BodyPart> getBodyparts() {
        return bodyparts;
    }

    public void addPart(BodyPart part) {
        bodyparts.add(part);
    }

    public void removePartType(BodyParts.BodyPartsEnum type, int number) {
        BodyPart ref;
        while(number > 0) {
            ref = null;
            for (BodyPart bodypart : bodyparts) {
                if(bodypart.getType() == type) {
                    ref = bodypart;
                }
            }
            if(ref == null) {
                break;
            }
            else {
                bodyparts.remove(ref);
            }
            number--;
        }
    }

    public void removeBodypart(BodyPart part) {
        bodyparts.remove(part);
    }

    @Override
    public int update() {
        return 0;
    }
}
