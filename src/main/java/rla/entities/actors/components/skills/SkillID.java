package rla.entities.actors.components.skills;

public enum SkillID {
    LIFE,
    MANA,
    LUCK,
    SPEED,
    STRENGTH,
    CONSTITUTION,
    DEXTERITY,
    PERCEPTION,
    WILLPOWER,
    LEARNING,
    MAGIC,
    CHARISMA,

}
