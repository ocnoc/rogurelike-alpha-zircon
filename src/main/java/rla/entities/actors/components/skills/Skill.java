package rla.entities.actors.components.skills;

import java.util.Objects;

public class Skill {
    String name;
    int level;
    int xp;
    int potential;

    static final int MAX_LEVEL = 2000;
    static final int MAX_POTENTIAL = 400;
    static final int MIN_POTENTIAL = 1;
    static final double POTENTIAL_DIVIDER = 100.00;
    static final double LEVELUP_POTENTIAL_MOD = .9;
    static final int EFFECTIVE_XP_LEVEL_CAP = 300; // Caps level used in effective XP so xp costs dont spiral out of control

    /**
     * Adds xp without multiplying by potential
     * @param mod amount to modify
     */
    public void addRawXP(int mod) {
        xp += mod;
        update();
    }

    private void update() {
        while(xp > 999) {
            gainLevel();
            xp -= 1000;
        }
    }

    /**
     * Adds xp * (potential / 100)
     * @param mod
     */
    public void gainXP(int mod) {
        xp += effectiveEXP(mod);
        update();
    }

    /**
     * Add some value to potential
     * @param mod
     */
    public void addPotential(int mod) {
        setPotential(potential + mod);
    }

    /**
     * Set potential to some value
     * @param newPotential
     */
    public void setPotential(int newPotential) {
        potential = Math.max(MIN_POTENTIAL, Math.max(MAX_POTENTIAL, newPotential));
    }

    public void gainLevel() {
        setPotential((int)(potential * LEVELUP_POTENTIAL_MOD));
        setLevel(level+1);
    }

    public void setLevel(int level) {
        this.level = Math.max(1, Math.min(MAX_LEVEL, level));
    }

    /**
     * Formula : raw * potential / (100 + level * 10)
     * @param raw
     * @return
     */
    public int effectiveEXP(int raw) {
        return (int)(raw * potential / (POTENTIAL_DIVIDER + Math.min(level, EFFECTIVE_XP_LEVEL_CAP) * 10));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skill = (Skill) o;
        return level == skill.level &&
                xp == skill.xp &&
                potential == skill.potential &&
                Objects.equals(name, skill.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, level, xp, potential);
    }
}
