package rla.entities.actors.components;

import rla.entities.RLAComponent;
import rla.entities.actors.components.skills.Skill;
import rla.entities.actors.components.skills.SkillID;

import java.util.HashMap;
import java.util.Map;

public class SkillComponent implements RLAComponent {
    Map<SkillID, Skill> skillList;

    public SkillComponent() {
        skillList = new HashMap<>();
    }

    @Override
    public int update() {
        return 0;
    }
}
