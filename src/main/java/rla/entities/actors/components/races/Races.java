package rla.entities.actors.components.races;

import rla.entities.actors.components.RaceComponent;

import java.util.ArrayList;
import java.util.List;

public class Races {
    public static List<RaceComponent> races;

    // TODO: Make a json to load in race data
    public static void initRaces() {
        races = new ArrayList<>();
        races.add(new RaceComponent("snail", "Slow and steady wins the race.", null));
    }
}
