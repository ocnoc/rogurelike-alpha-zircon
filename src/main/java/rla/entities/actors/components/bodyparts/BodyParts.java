package rla.entities.actors.components.bodyparts;

public class BodyParts {
    public enum BodyPartsEnum {
        HEAD("Head"),
        BODY("Body"),
        HAND("Hand"),
        ARM("Arm"),
        LEG("Leg"),
        NECK("Neck"),
        BAKC("Back"),
        RING("Ring"),
        WAIST("Waist");

        final String partName;

        BodyPartsEnum(String partName) {
            this.partName = partName;
        }
    }
}
