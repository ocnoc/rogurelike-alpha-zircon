package rla.entities.actors.components.bodyparts;

import rla.entities.Entity;
import rla.entities.ItemContainer;
import rla.entities.items.Inventory;
import rla.entities.items.Item;

public class BodyPart implements ItemContainer, Entity {
    BodyParts.BodyPartsEnum type;
    private Inventory equipped;

    public BodyPart(BodyParts.BodyPartsEnum type) {
        this.type = type;
        equipped = new Inventory(1);
    }

    public String getName() {
        return type.partName;
    }

    public BodyParts.BodyPartsEnum getType() {
        return type;
    }

    @Override
    public Inventory getInventory() {
        return equipped;
    }
}
