package rla.entities.actors.components;

import rla.entities.RLAComponent;
import rla.world.Map;

public class LocationComponent implements RLAComponent {
    private int x;
    private int y;
    private Map map;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public LocationComponent(int x, int y, Map map) {
        this.x = x;
        this.y = y;
        this.map = map;
    }

    @Override
    public int update() {
        return 0;
    }
}
