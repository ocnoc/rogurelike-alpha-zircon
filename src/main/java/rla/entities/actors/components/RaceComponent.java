package rla.entities.actors.components;

import rla.entities.RLAComponent;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.entities.actors.components.bodyparts.BodyParts;

import java.util.List;

public class RaceComponent implements RLAComponent {
    private String name;
    private String description;
    private List<BodyParts.BodyPartsEnum> defaultBodyparts;

    public RaceComponent(String name, String description, List<BodyParts.BodyPartsEnum> defaultBodyparts) {
        this.name = name;
        this.description = description;
        this.defaultBodyparts = defaultBodyparts;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<BodyParts.BodyPartsEnum> getDefaultBodyparts() {
        return defaultBodyparts;
    }

    @Override
    public int update() {
        return 0;
    }
}
