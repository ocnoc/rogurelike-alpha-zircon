package rla.entities.actors.components;

import rla.entities.RLAComponent;
import rla.entities.actors.Actor;

public class HungerComponent implements RLAComponent {
    private Actor actor;
    private final int FULL_AMOUNT = 1000;
    private final int HUNGER_TICK = 1;
    private final int STARTING_HUNGER = 500;
    private int fullness;

    public HungerComponent(Actor actor) {
        this.actor = actor;
        fullness = STARTING_HUNGER;
    }

    public boolean full() {
        if(fullness >= FULL_AMOUNT) {
            return true;
        }
        return false;
    }

    public String getHungerState() {
        if(fullness >= FULL_AMOUNT ) {
            return "Bloated";
        }
        else if(fullness >= FULL_AMOUNT * .8) {
            return "Satisfied";
        }
        else if(fullness >= FULL_AMOUNT * .3) {
            return "";
        }
        else if(fullness >= FULL_AMOUNT * .1) {
            return "Hungry";
        }
        else {
            return "Starving!";
        }
    }

    public void modifyFullness(int mod) {
        fullness = Math.min(Math.max(fullness + mod, 0), FULL_AMOUNT);
    }

    @Override
    public int update() {
        modifyFullness(-1 * HUNGER_TICK);
        if(fullness <= 0) {
            actor.modifyHp(-1);
        }
        return 0;
    }
}
