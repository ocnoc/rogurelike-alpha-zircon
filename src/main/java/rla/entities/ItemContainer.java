package rla.entities;

import rla.entities.items.Inventory;

public interface ItemContainer {
    Inventory getInventory();
}
