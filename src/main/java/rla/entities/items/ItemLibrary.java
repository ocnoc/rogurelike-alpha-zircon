package rla.entities.items;

import rla.entities.items.components.UseComponent;
import rla.entities.items.components.onuse.StairUse;

import java.util.ArrayList;
import java.util.List;

public class ItemLibrary {
    private static List<Item> items;

    public enum ItemTemplates {
        STAIRS_DOWN(1),
        STAIRS_UP(2);

        public int itemid;

        ItemTemplates(int i) {
            this.itemid = i;
        }
    }

    public static void init() {
        items = new ArrayList<>();
        items.add(new Item('t', "torch", new int[]{255, 0, 0}));

        Item stairsDown = new Item('>', "stairs down", new int[]{255, 0, 0});
        stairsDown.registerComponent(new UseComponent(new StairUse(true)));
        items.add(stairsDown);

        Item stairsUp = new Item('<', "stairs up", new int[]{255, 0, 0});
        stairsUp.registerComponent(new UseComponent(new StairUse(false)));
        items.add(stairsUp);
    }

    public static Item getTorch() {
        try {
            return (Item) items.get(0).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Item getItem(int id) {
        try {
            return (Item) items.get(id).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Item getItem(String name) {
        try {
            for(Item item : items) {
                if(item.getName().equals(name)) {
                    return (Item)item.clone();
                }
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
