package rla.entities.items;

import rla.entities.ItemContainer;

public class Items {

    public static boolean moveItem(ItemContainer srcContainer, Item item, ItemContainer destContainer) {
        if(destContainer.getInventory().add(item)) {
            if (srcContainer != null) {
                srcContainer.getInventory().removeItem(item);
            }
            item.setParent(destContainer);
            return true;
        }
        return false;
    }

    public static void removeItem(ItemContainer srcContainer, Item item) {
        srcContainer.getInventory().removeItem(item);
        item.setParent(null);
    }

    public static boolean addItem(Item item, ItemContainer destContainer) {
        if(destContainer.getInventory().add(item)) {
            item.setParent(destContainer);
            return true;
        }
        return false;
    }
}
