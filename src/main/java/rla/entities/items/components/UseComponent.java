package rla.entities.items.components;

import rla.entities.RLAComponent;
import rla.entities.items.components.onuse.OnUseEffect;
import rla.states.PlayState;

public class UseComponent implements RLAComponent {
    OnUseEffect effect;

    public UseComponent(OnUseEffect effect) {
        this.effect = effect;
    }

    public boolean use(PlayState state) {
        return effect.use(state);
    }

    @Override
    public int update() {
        return 0;
    }
}
