package rla.entities.items.components;

import rla.entities.RLAComponent;

public class FoodComponent implements RLAComponent {
    private int nutritionValue;

    public FoodComponent(int nutritionValue) {
        this.nutritionValue = nutritionValue;
    }
    @Override
    public int update() {
        return 0;
    }

    public int getNutritionValue() {
        return nutritionValue;
    }

    public void setNutritionValue(int nutritionValue) {
        this.nutritionValue = nutritionValue;
    }
}
