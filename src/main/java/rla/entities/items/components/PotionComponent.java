package rla.entities.items.components;

import rla.entities.RLAComponent;
import rla.entities.actors.Actor;
import rla.entities.items.Item;
import rla.entities.spells.SpellEffect;
import rla.tiles.Tile;

public class PotionComponent implements RLAComponent {
    private SpellEffect effect;

    public PotionComponent(SpellEffect effect) {
        this.effect = effect;
    }

    public boolean apply(Actor caster, Actor target) {
        return effect.apply(caster, target);
    }

    public boolean apply(Actor caster, Tile target) {
        return effect.apply(caster, target);
    }

    public boolean apply(Actor caster, Item target) {
        return effect.apply(caster, target);
    }

    @Override
    public int update() {
        return 0;
    }
}
