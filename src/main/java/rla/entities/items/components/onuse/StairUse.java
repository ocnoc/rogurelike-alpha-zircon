package rla.entities.items.components.onuse;

import rla.entities.actors.Actor;
import rla.states.GameState;
import rla.states.PlayState;
import rla.world.World;

public class StairUse implements OnUseEffect {
    boolean down;
    @Override
    public boolean use(PlayState state) {
        World world = state.getWorld();
        if(world.getCurrFloor() <= 0 && !down) {
            state.getPlayer().notify("You cannot leave the dungeon!");
            return false;
        }
        world.moveFloor(down ? world.getCurrFloor() + 1 : world.getCurrFloor() - 1);
        state.getPlayer().notify("You move floors.");
        return true;
    }

    public StairUse(boolean down) {
        this.down = down;
    }
}
