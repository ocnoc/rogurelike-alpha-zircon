package rla.entities.items.components.onuse;

import rla.states.PlayState;

public interface OnUseEffect {
    boolean use(PlayState state);
}
