package rla.entities.items;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    List<Item> items;
    private int localMaxItems;
    private final int MAX_ITEMS = Integer.MAX_VALUE;

    public Inventory() {
        this.items = new ArrayList<>();
        localMaxItems = -1;
    }

    public Inventory(int localMaxItems) {
        this.items = new ArrayList<>();
        this.localMaxItems = localMaxItems;
    }

    public Item getItem(int id) {
        if(id >= items.size()) {
            return new Item('E', "error", new int[]{0, 0, 0});
        }
        return items.get(id);
    }

    public Item getItem(Item item) {
        return getItem(items.indexOf(item));
    }

    public List<Item> getItemList() {
        return items;
    }

    protected boolean add(Item item) {
        if(isFull()) {
            return false;
        }
        items.add(item);
        return true;
    }

    public boolean isFull() {
        return localMaxItems == -1 ? items.size() >= MAX_ITEMS : items.size() >= localMaxItems;
    }

    boolean removeItem(Item item) {
        items.remove(item);
        return true;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }
}
