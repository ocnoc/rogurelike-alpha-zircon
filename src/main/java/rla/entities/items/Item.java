package rla.entities.items;

import rla.entities.Entity;
import rla.entities.ItemContainer;
import rla.entities.RLAComponent;

import java.util.ArrayList;
import java.util.List;

public class Item implements Cloneable, Entity {
    private char glyph;
    private String name;
    private int[] color;
    private ItemContainer parent;

    private List<RLAComponent> components;

    public RLAComponent getComponent(Class<? extends RLAComponent> clazz) {
        for(RLAComponent component : components) {
            if(component.getClass() == clazz) {
                return component;
            }
        }
        return null;
    }

    public boolean registerComponent(RLAComponent component) {
        if(getComponent(component.getClass()) == null) {
            components.add(component);
            return true;
        }
        return false;
    }

    public Item(char glyph, String name, int[] color) {
        this.glyph = glyph;
        this.name = name;
        this.color = color;
        components = new ArrayList<>();
    }

    public char getGlyph() {
        return glyph;
    }

    public void setGlyph(char glyph) {
        this.glyph = glyph;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getColor() {
        return color;
    }

    public void setColor(int[] color) {
        this.color = color;
    }

    public boolean isDroppable() {
        return true;
    }

    public ItemContainer getParent() {
        return parent;
    }

    protected void setParent(ItemContainer parent) {
        this.parent = parent;
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
}
