package rla.entities.timing;

import rla.actions.Action;
import rla.entities.actors.Actor;

import java.sql.Time;
import java.util.*;

public class TimeSentinel {
    private List<TimeBasedEntity> timedEntities;
    private Queue<TimeBasedEntity> queuedEntities;
    private Queue<TimeBasedEntity> deregisterQueue;
    private TimeBasedEntity player;
    private static final int ACTION_THRESHOLD = 1000;
    private static  TimeSentinel sentinel;

    public static TimeSentinel getTimeSentinel() {
        if(sentinel == null) {
            sentinel = new TimeSentinel();
        }
        return sentinel;
    }

    private TimeSentinel() {
        queuedEntities = new ArrayDeque<>();
        timedEntities = new LinkedList<>();
        deregisterQueue = new ArrayDeque<>();
    }

    public void register(TimeBasedEntity entity) {
        if(!timedEntities.contains(entity)) {
            entity.setActionPoints(0);
            timedEntities.add(entity);
        }
    }

    public void registerPlayer(TimeBasedEntity entity) {
        player = entity;
        register(entity);
    }

    public void release(TimeBasedEntity e) {
        if(e == player) {
            return;
        }
        timedEntities.remove(e);
        queuedEntities.remove(e);
    }

    public boolean contains(TimeBasedEntity e) {
        return timedEntities.contains(e);
    }

    public void tick() {
        while(true) {
            while(deregisterQueue.size() > 0) {
                TimeBasedEntity entity = deregisterQueue.poll();
                release(entity);
            }
            while (queuedEntities.size() > 0) {
                TimeBasedEntity entity = queuedEntities.poll();
                entity.addActionPoints(-1 * ACTION_THRESHOLD);
                if(entity.toDeregister()) {
                    release(entity);
                    continue;
                }
                if(entity == player) {
                    return;
                }
                entity.update();
            }
            if (timedEntities.size() > 0) {
                for(TimeBasedEntity e : timedEntities) {
                    if(e.toDeregister()) {
                        deregisterQueue.add(e);
                        continue;
                    }
                    e.addActionPoints(e.getSpeed());
                    if(e.getActionPoints() >= ACTION_THRESHOLD) {
                        queuedEntities.add(e);
                    }
                }
            }
        }
    }
}
