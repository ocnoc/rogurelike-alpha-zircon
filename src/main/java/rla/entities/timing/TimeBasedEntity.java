package rla.entities.timing;

public interface TimeBasedEntity {
    int update();
    int getSpeed();
    void addActionPoints(int i);
    int getActionPoints();
    void setActionPoints(int i);
    boolean toDeregister();
}
