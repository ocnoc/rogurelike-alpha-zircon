package rla.entities.spells;

import rla.entities.actors.Actor;
import rla.entities.items.Item;
import rla.tiles.Tile;

public abstract class SpellEffect {
    protected int spellPower;

    public SpellEffect(int spellPower) {
        this.spellPower = spellPower;
    }

    public boolean apply(Actor caster, Actor target) {
        return false;
    }

    public boolean apply(Actor caster, Tile target) {
        return false;
    }

    public boolean apply(Actor caster, Item target) {
        return false;
    }
}
