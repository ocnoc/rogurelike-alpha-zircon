package rla.entities.spells.effects;

import rla.entities.actors.Actor;
import rla.entities.spells.SpellEffect;
import rla.utilities.Utilities;
import squidpony.squidmath.Dice;

public class HealHPSpellEffect extends SpellEffect {

    public HealHPSpellEffect(int spellPower) {
        super(spellPower);
    }


    public boolean apply(Actor caster, Actor target) {
        int dice = 1;
        int sides = (7 + (spellPower / 4));
        int healAmount = new Dice().rollDice(dice, sides);
        target.modifyHp(healAmount);
        target.doAction("heal for " + healAmount + " hp");
        return true;
    }
}
