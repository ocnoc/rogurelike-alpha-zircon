package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;

import java.util.ArrayList;
import java.util.List;

/**
 * Beginnings of a state manager to replace the current system of returning a new state on user input.
 */
public class StateManager {
    private static List<GameState> stateStack = new ArrayList<>();

    public static void update(KeyStroke key) {
        stateStack.get(0).update(key);
    }

    public static void render(DrawSurface terminal) {
        List<GameState> renderStack = new ArrayList<>();
        renderStack.add(stateStack.get(0));
        int i = 0;
        while(renderStack.get(i).renderBelowState()) {
            renderStack.add(stateStack.get(++i));
        }
        for(int j=renderStack.size()-1;j>=0;j--) {
            renderStack.get(j).render(terminal);
        }
    }

    public static GameState stackState(GameState state) {
        stateStack.add(0, state);
        return state;
    }

    public static GameState popState() {
        return stateStack.remove(0);
    }

    public static GameState setState(GameState state) {
        stateStack = new ArrayList<>();
        stateStack.add(state);
        return state;
    }
}
