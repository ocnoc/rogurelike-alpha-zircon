package rla.states;

import org.hexworks.zircon.api.*;
import org.hexworks.zircon.api.builder.component.ParagraphBuilder;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.component.LogArea;
import org.hexworks.zircon.api.component.Panel;
import org.hexworks.zircon.api.component.TextArea;
import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.grid.TileGrid;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;
import rla.actions.Action;
import rla.actions.Actions;
import rla.displays.TerminalDisplay;
import rla.entities.actors.Actor;
import rla.entities.actors.components.HungerComponent;
import rla.entities.items.ItemLibrary;
import rla.entities.timing.TimeSentinel;
import rla.states.rendering.PlayStateRenderer;
import rla.states.rendering.StateRenderer;
import rla.utilities.MessageDisplayer;
import rla.utilities.MessageInterface;
import rla.utilities.MessageInterfaceLocator;
import rla.world.World;

public class PlayState implements GameState, MessageDisplayer {
    public static final int SCREEN_OFFSET = 6; // screen is offset this much on y axis to give room for stats/messages. number of messages shown is this-1, since we need the stat line
    private final String PLAYER_STATS_DISPLAY = "HP: %2d/%2d | %s";
    private final ColorTheme INFOBOX_THEME = TerminalDisplay.COMPONENT_THEME;
    private final long MESSAGE_TYPING_TIME = 0;
    private final TimeSentinel sentinel;

    private World world;
    private Actor player;
    private int screenWidth;
    private int screenHeight;
    private Screen playScreen;
    private TileGrid terminal;
    private Action multiStep;
    private StateRenderer renderer;
    private MessageInterface messageLog;

    /* Zircon Screen Components */
    Panel infoPanel;
    TextArea statsLabel;
    LogArea messageDisplay;


    public PlayState(int screenWidth, int screenHeight, TileGrid terminal) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight-SCREEN_OFFSET; // We store screenheight as being -OFFSET to save room for messages/stats
        messageLog = MessageInterfaceLocator.getMessageLog();
        sentinel = TimeSentinel.getTimeSentinel();
        createWorld();
        initializePlayer();
        initializeScreen(terminal);
        messageLog.setDisplay(this);
        renderer = new PlayStateRenderer(this, playScreen, player);
        playScreen.display();
    }

    public void initializeScreen(TileGrid grid) {
        this.terminal = grid;
        this.playScreen = Screens.createScreenFor(grid);
        playScreen.setCursorVisibility(false);

        infoPanel = Components.panel().
                withPosition(Positions.create(1, screenHeight)).
                withSize(Sizes.create(screenWidth-2, SCREEN_OFFSET)).
                build();

        statsLabel = Components.textArea().
                withPosition(Positions.create(0, 0)).
                withSize(Sizes.create(infoPanel.getWidth(), 1)).
                build();
        infoPanel.addComponent(statsLabel);

        messageDisplay = Components.logArea().
                withLogRowHistorySize(1).
                withPosition(Positions.create(0, 0).relativeToBottomOf(statsLabel)).
                withSize(Sizes.create(infoPanel.getWidth(), SCREEN_OFFSET-1)).
                build();
        infoPanel.addComponent(messageDisplay);
        infoPanel.applyColorTheme(INFOBOX_THEME);

        playScreen.addComponent(infoPanel);
    }
    /**
     * Creates a new world object to hold our maps
     */
    private void createWorld() {

        world = new World(sentinel);
    }

    public World getWorld() {
        return world;
    }

    private void initializePlayer() {
        player = world.getPlayer();
        player.getItem(ItemLibrary.getTorch());
    }

    @Override
    public boolean renderBelowState() {
        return false;
    }

    /**
     * displays tiles, actors and player stats
     * @param surface
     */
    @Override
    public void render(DrawSurface surface) {
        renderer.render();
        displayStats();
    }

    public void displayStats() {
        String stats = String.format(PLAYER_STATS_DISPLAY, player.getHp(), player.getMaxhp(), ((HungerComponent)player.getComponent(HungerComponent.class)).getHungerState());
        statsLabel.setText(stats);
    }

    /**
     * Magic function that calls update on everything else. Public so other states can call it.
     */
    public void update() {
        world.update();
    }

    public Actor getPlayer() { return player;}

    public void displayMessages() {
        while(messageLog.hasNext()) {
            messageDisplay.addParagraph(new ParagraphBuilder().
                    withTypingEffect(MESSAGE_TYPING_TIME).
                    withText(messageLog.popOldestMessage()).
                    withComponentStyleSet(infoPanel.getComponentStyleSet()),
                    false);
        }
    }

    /**
     * Accepts a keypress and proceeds with an action based on that keypress.
     * @param key user's key press
     * @return the screen after acting
     */
    @Override
    public void update(KeyStroke key) {
        GameState transitionState = null;
        boolean validAction = false; // we only want to update everything if we've done something valid, otherwise dont call update
        if(multiStep != null) {
            validAction = multiStep.perform(this, key);
            transitionState = multiStep.getTransitionState();
            multiStep = null;
        }
        else {
            multiStep = Actions.getActions().getAction(this.getClass(), key).orElse(Actions.getInvalidAction());
            if(!(multiStep.isMultiStep())) {
                validAction = multiStep.perform(this);
                transitionState = multiStep.getTransitionState();
                multiStep = null;
            }
            else {
                multiStep.perform(this); // Added in case multi-steps message player further instructions
            }
        }
        if(validAction) {
            player.update();
            sentinel.tick(); // update the map after processing input if we didnt do something like try to walk into a wall
        }
        if(transitionState != null) {
            StateManager.stackState(transitionState);
        }
    }

    public void setRenderer(StateRenderer r) {
        this.renderer = r;
    }

    public Screen getPlayScreen() {
        return playScreen;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }
}
