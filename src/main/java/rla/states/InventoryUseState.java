package rla.states;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.entities.items.components.PotionComponent;
import rla.entities.items.components.UseComponent;

public class InventoryUseState extends InventoryState{
    public InventoryUseState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        super(actor, items, priorState, screen);
    }

    @Override
    public String getVerb() {
        return "use";
    }

    @Override
    public boolean isValid(Item item) {
        return item.getComponent(UseComponent.class) != null;
    }

    @Override
    public GameState use(Item item) {
        UseComponent use = ((UseComponent)item.getComponent(UseComponent.class));
        use.use((PlayState)priorState);
        actor.doAction("use %s", item.getName());
        doneOverride = true;
        return this;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }
}
