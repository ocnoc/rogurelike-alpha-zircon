package rla.states;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.entities.items.Items;
import rla.entities.items.components.PotionComponent;

public class InventoryQuaffState extends InventoryState{
    public InventoryQuaffState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        super(actor, items, priorState, screen);
    }

    @Override
    public String getVerb() {
        return "quaff";
    }

    @Override
    public boolean isValid(Item item) {
        return item.getComponent(PotionComponent.class) != null;
    }

    @Override
    public GameState use(Item item) {
        PotionComponent potion = ((PotionComponent)item.getComponent(PotionComponent.class));
        potion.apply(actor, actor);
        Items.removeItem(item.getParent(), item);
        actor.doAction("drink %s", item.getName());
        return this;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }
}
