package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;
import rla.actions.Action;
import rla.actions.Actions;
import rla.entities.actors.Actor;
import rla.entities.actors.components.BodyComponent;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.entities.items.Item;
import rla.entities.items.Items;
import rla.states.rendering.BodyViewRenderer;
import rla.states.rendering.SingleListRenderer;

import java.util.ArrayList;
import java.util.List;

public class BodyWearState implements GameState, SelectableList {
    Actor actor;
    BodyViewRenderer renderer;
    List<BodyPart> validItems;
    int page;
    boolean doneOverride;

    //public abstract boolean isValid(Item item);
    public void use(BodyPart part) {
        endMenu();
        if(!part.getInventory().isEmpty()) {
            ((BodyComponent)actor.getComponent(BodyComponent.class)).unequip(part);
            return;
        }
        StateManager.stackState(new InventoryWearState(actor, actor.getInventory(), this, renderer.getScreen(), part));
    }

    public void use(int index) {
        index = index + (page * (renderer).ITEMS_PER_PAGE);
        if(checkValidItems().size() <= index) {
            return;
        }
        BodyPart part = validItems.get(index);
        use(part);
    }

    public BodyWearState(Actor actor, Screen screen) {
        this.actor = actor;
        this.renderer = new BodyViewRenderer(screen, this);
    }

    public void changePage(int i) {
        page += i;
        if(page < 0) {
            page = renderer.calculateMaxPages();
        }
        if(page > renderer.calculateMaxPages()) {
            page = 0;
        }
        renderer.changePage(page);
    }

    @Override
    public void update(KeyStroke key) {
        Action action = Actions.getActions().getAction(this.getClass(), key).orElse(Actions.getInvalidAction());
        boolean done;
        doneOverride = false;
        if(action.isMultiStep()) {
            done = action.perform(this, key);
        }
        else {
            done = action.perform(this);
        }
        if(done || doneOverride || checkValidItems().isEmpty()) {
            endMenu();
        }
    }

    public void endMenu() {
        renderer.closeMenu();
        StateManager.popState();
    }

    public List<BodyPart> checkValidItems() {
        validItems = new ArrayList<>();
        if(actor.getComponent(BodyComponent.class) != null) {
            BodyComponent bc = (BodyComponent) actor.getComponent(BodyComponent.class);
            validItems.addAll(bc.getBodyparts());
        }
        return validItems;
    }

    public void render(DrawSurface display) {
        renderer.render();
    }

    public boolean renderBelowState() {
        return true;
    }
}
