package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;

public interface GameState {
    boolean renderBelowState();
    void render(DrawSurface display);
    void update(KeyStroke key);
}
