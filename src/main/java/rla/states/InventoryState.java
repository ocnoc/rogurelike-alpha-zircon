package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;
import rla.actions.Action;
import rla.actions.Actions;
import rla.entities.Entity;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.states.rendering.InventoryRenderer;
import rla.states.rendering.StateRenderer;

import java.util.ArrayList;
import java.util.List;

public abstract class InventoryState implements GameState, SelectableList{
    Inventory items;
    Actor actor;
    GameState priorState;
    StateRenderer renderer;
    List<Item> validItems;
    int page;
    boolean doneOverride;
    Item selectedItem;

    public abstract String getVerb();
    public abstract boolean isValid(Item item);
    public abstract GameState use(Item item);
    public void use(int index) {
        index = index + (page * ((InventoryRenderer)renderer).ITEMS_PER_PAGE);
        if(checkValidItems().size() <= index) {
            return;
        }
        Item item = validItems.get(index);
        use(item);
    }
    public abstract GameState getPriorState();

    public InventoryState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        this.actor = actor;
        this.items = items;
        this.priorState = priorState;
        this.renderer = new InventoryRenderer(screen, this);
    }

    public void changePage(int i) {
        page += i;
        if(page < 0) {
            page = ((InventoryRenderer)renderer).calculateMaxPages();
        }
        if(page > ((InventoryRenderer)renderer).calculateMaxPages()) {
            page = 0;
        }
        ((InventoryRenderer)renderer).changePage(page);
    }

    @Override
    public void update(KeyStroke key) {
        Action action = Actions.getActions().getAction(this.getClass(), key).orElse(Actions.getInvalidAction());
        boolean done;
        doneOverride = false;
        if(action.isMultiStep()) {
            done = action.perform(this, key);
        }
        else {
            done = action.perform(this);
        }
        if(done || doneOverride || checkValidItems().isEmpty()) {
            endMenu();
        }
    }

    public void endMenu() {
        ((InventoryRenderer)renderer).closeMenu();
        StateManager.popState();
    }

    public List<Item> checkValidItems() {
        validItems = new ArrayList<>();
        items.getItemList().forEach(i -> {
            if(isValid(i)) {
                validItems.add(i);
            }
        });
        return validItems;
    }

    public void render(DrawSurface display) {
        renderer.render();
    }

    public boolean renderBelowState() {
        return true;
    }
}
