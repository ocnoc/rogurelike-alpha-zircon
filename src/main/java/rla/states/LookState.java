package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;
import rla.actions.Action;
import rla.actions.Actions;
import rla.entities.actors.Actor;
import rla.states.rendering.LookStateRenderer;
import rla.states.rendering.StateRenderer;

public class LookState implements GameState {
    Actor actor;
    GameState priorState;
    StateRenderer renderer;

    public LookState(Actor actor, GameState priorState, Screen screen) {
        int width = ((PlayState)priorState).getScreenWidth();
        int height = ((PlayState)priorState).getScreenHeight();
        this.actor = actor;
        this.priorState = priorState;
        this.renderer = new LookStateRenderer(actor, this, screen, width, height);
    }

    @Override
    public boolean renderBelowState() {
        return true;
    }

    @Override
    public void render(DrawSurface terminal) {
        renderer.render();
    }

    public void move(int mx, int my) {
        ((LookStateRenderer)renderer).move(mx, my);
    }

    @Override
    public void update(KeyStroke key) {
        Action action = Actions.getActions().getAction(this.getClass(), key).orElse(Actions.getInvalidAction());
        boolean done;
        if(action.isMultiStep()) {
            done = action.perform(this, key);
        }
        else {
            done = action.perform(this);
        }
        if(done) {
            StateManager.popState();
        }
    }
}
