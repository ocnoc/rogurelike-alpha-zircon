package rla.states.rendering;

public interface StateRenderer {
    void render();
}
