package rla.states.rendering;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.Entity;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.states.BodyWearState;
import rla.states.GameState;

import java.util.ArrayList;
import java.util.List;

public class BodyViewRenderer extends SingleListRenderer {

    public Screen getScreen() {
        return this.baseScreen;
    }

    @Override
    String getPanelTitle() {
        return "Body";
    }

    @Override
    public void use(Entity item) {

    }

    @Override
    public List<Entity> checkValidItems() {
        return new ArrayList<>(((BodyWearState)state).checkValidItems());
    }

    @Override
    String getItemDisplayInfo(Entity item) {
        StringBuilder message = new StringBuilder();
        message.append(((BodyPart)item).getName());
        if(!((BodyPart)item).getInventory().isEmpty()) {
            message.append(" - ").append(((BodyPart) item).getInventory().getItem(0).getName());
        }
        return message.toString();
    }

    public BodyViewRenderer(Screen baseScreen, GameState state) {
        super(baseScreen, state);
    }
}
