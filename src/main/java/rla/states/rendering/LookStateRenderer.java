package rla.states.rendering;

import org.hexworks.zircon.api.Modifiers;
import org.hexworks.zircon.api.Positions;
import org.hexworks.zircon.api.TileColors;
import org.hexworks.zircon.api.Tiles;
import org.hexworks.zircon.api.builder.graphics.TileGraphicsBuilder;
import org.hexworks.zircon.api.color.TileColor;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.graphics.TileGraphics;
import org.hexworks.zircon.api.screen.Screen;
import rla.displays.TerminalDisplay;
import rla.entities.actors.Actor;
import rla.states.GameState;
import rla.states.LookState;
import rla.states.PlayState;
import rla.tiles.Tile;
import rla.utilities.FieldOfView;
import rla.world.World;

public class LookStateRenderer implements StateRenderer{
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    private final double MIN_LIGHTING = .6;

    private GameState playState;
    private Screen playScreen;
    private Actor player;

    private int tx;
    private int ty;

    public LookStateRenderer(Actor actor, GameState state, Screen playScreen, int screenWidth, int screenHeight) {
        this.playState = state;
        this.playScreen = playScreen;
        this.SCREEN_WIDTH = screenWidth;
        this.SCREEN_HEIGHT = screenHeight;
        player = actor;
        tx = player.getX();
        ty = player.getY();
    }

    @Override
    public void render() {
        TileGraphics buffer = new TileGraphicsBuilder().withSize(playScreen.getSize()).build();
        int left = getScrollX();
        int top = getScrollY();
        displayTiles(buffer, left, top);
        playScreen.draw(buffer, Positions.defaultPosition());
    }

    public void drawTile(DrawSurface surface, int x, int y, char glyph, int[] color, double darkenPercent) {
        drawTile(surface, x, y, glyph, color, darkenPercent, false);
    }

    public void drawTile(DrawSurface surface, int x, int y, char glyph, int[] color, double darkenPercent, boolean target) {
        final TileColor targetColor = TileColors.create(0, 0, 255);
        TileColor tcolor = TileColors.create(color[0], color[1], color[2]).darkenByPercent(darkenPercent);
        org.hexworks.zircon.api.data.Tile t = Tiles.newBuilder().
                                                    withCharacter(glyph).
                                                    withForegroundColor(tcolor).
                                                    withBackgroundColor(target ? targetColor : TileColors.defaultBackgroundColor()).
                                                    build();
        surface.setTileAt(Positions.create(x, y), t);

    }

    private void displayTiles(TileGraphics terminal, int left, int top) {
        player.updateFOV();
        FieldOfView fov = player.getFov();
        // Loops through drawing every visible tile
        for(int x=0; x < SCREEN_WIDTH; x++) {
            for(int y=0; y < SCREEN_HEIGHT; y++) {
                int wx = x + left;
                int wy = y + top;
                Tile currTile = fov.getMemory(wx, wy, player.getMap().getTileMap());
                double darkenPercent = MIN_LIGHTING * (1 - fov.getVisibility(wx, wy));
                char glyph = currTile.getInventory().isEmpty() ? currTile.getGlyph() : currTile.getInventory().getItem(0).getGlyph();
                int[] color = currTile.getInventory().isEmpty() ? currTile.getColor() : currTile.getInventory().getItem(0).getColor();
                drawTile(terminal, x, y, glyph, color, darkenPercent, (tx == wx && ty == wy));
            }
        }
        // Loops through every actor afterwards. if they're in our field of view, we draw them.
        player.getMap().getActors().getActorList().forEach(a -> {
            if(a.getX() >= left && a.getX() < left + SCREEN_WIDTH && a.getY() >= top && a.getY() < top + SCREEN_HEIGHT && fov.getVisibility(a.getX(), a.getY()) > 0) {
                double darkenPercent = MIN_LIGHTING * (1- fov.getVisibility(a.getX(), a.getY()));
                int wx = a.getX() - left;
                int wy = a.getY() - top;
                drawTile(terminal, wx, wy, a.getGlyph(), a.getColor(), darkenPercent, (tx == wx + left && ty == wy + top));
            }
        });
    }

    public void move(int mx, int my) {
        tx = tx + mx;
        ty = ty + my;
    }

    /**
     * limits horizontal scrolling. The max makes it so we either are stuck on edge or scroll freely
     * the min decides if we're close enough to the right edge
     * @return
     */
    public int getScrollX() {
        return Math.max(0,
                Math.min(tx-SCREEN_WIDTH / 2,
                        player.getMap().getWidth() - SCREEN_WIDTH));
    }

    /**
     * limits vertical scrolling
     * @return
     */
    public int getScrollY() {
        return Math.max(0, Math.min(ty-SCREEN_HEIGHT / 2, player.getMap().getHeight() - SCREEN_HEIGHT));
    }
}
