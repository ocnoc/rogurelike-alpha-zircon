package rla.states.rendering;

import org.hexworks.zircon.api.Positions;
import org.hexworks.zircon.api.TileColors;
import org.hexworks.zircon.api.Tiles;
import org.hexworks.zircon.api.builder.graphics.TileGraphicsBuilder;
import org.hexworks.zircon.api.color.TileColor;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.graphics.TileGraphics;
import org.hexworks.zircon.api.resource.ColorThemeResource;
import org.hexworks.zircon.api.screen.Screen;
import rla.displays.TerminalDisplay;
import rla.entities.actors.Actor;
import rla.states.GameState;
import rla.states.PlayState;
import rla.tiles.Tile;
import rla.utilities.FieldOfView;
import rla.world.World;

public class PlayStateRenderer implements StateRenderer{
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    private final int SCREEN_OFFSET = 6; // screen is offset this much on y axis to give room for stats/messages. number of messages shown is this-1, since we need the stat line
    private final String PLAYER_STATS_DISPLAY = "HP: %2d/%2d | ATK : %2d | DEF %2d";
    private final double MIN_LIGHTING = .6;
    private final ColorTheme INFOBOX_THEME = TerminalDisplay.COMPONENT_THEME;

    private GameState playState;
    private Screen playScreen;
    private Actor player;

    public PlayStateRenderer(GameState state, Screen playScreen, Actor player) {
        this.playState = state;
        this.playScreen = playScreen;
        this.SCREEN_WIDTH = ((PlayState)state).getScreenWidth();
        this.SCREEN_HEIGHT = ((PlayState)state).getScreenHeight();
        this.player = player;
    }

    @Override
    public void render() {
        TileGraphics buffer = new TileGraphicsBuilder().withSize(playScreen.getSize()).build();
        int left = getScrollX();
        int top = getScrollY();
        displayTiles(buffer, left, top);
        ((PlayState)playState).displayMessages();
        playScreen.draw(buffer, Positions.defaultPosition());
    }

    public void drawTile(DrawSurface surface, int x, int y, char glyph, int[] color, double darkenPercent) {
        TileColor tcolor = TileColors.create(color[0], color[1], color[2]).darkenByPercent(darkenPercent);
        surface.setTileAt(Positions.
                        create(x, y),
                Tiles.newBuilder().
                        withCharacter(glyph).
                        withForegroundColor(tcolor).
                        withBackgroundColor(TileColors.defaultBackgroundColor()).
                        build());
    }

    private void displayTiles(TileGraphics terminal, int left, int top) {
        player.updateFOV();
        FieldOfView fov = player.getFov();
        // Loops through drawing every visible tile
        for(int x=0; x < SCREEN_WIDTH; x++) {
            for(int y=0; y < SCREEN_HEIGHT; y++) {
                int wx = x + left;
                int wy = y + top;
                Tile currTile = fov.getMemory(wx, wy, player.getMap().getTileMap());
                double darkenPercent = MIN_LIGHTING * (1 - fov.getVisibility(wx, wy));
                char glyph = currTile.getInventory().isEmpty() ? currTile.getGlyph() : currTile.getInventory().getItem(0).getGlyph();
                int[] color = currTile.getInventory().isEmpty() ? currTile.getColor() : currTile.getInventory().getItem(0).getColor();
                drawTile(terminal, x, y, glyph, color, darkenPercent);
            }
        }
        // Loops through every actor afterwards. if they're in our field of view, we draw them.
        player.getMap().getActors().getActorList().forEach(a -> {
            if(a.getX() >= left && a.getX() < left + SCREEN_WIDTH && a.getY() >= top && a.getY() < top + SCREEN_HEIGHT && fov.getVisibility(a.getX(), a.getY()) > 0) {
                double darkenPercent = MIN_LIGHTING * (1- fov.getVisibility(a.getX(), a.getY()));
                drawTile(terminal, a.getX()-left, a.getY()-top, a.getGlyph(), a.getColor(), darkenPercent);
            }
        });
    }

    /**
     * limits horizontal scrolling. The max makes it so we either are stuck on left edge or scroll freely
     * the min decides if we're close enough to the right edge
     * @return
     */
    public int getScrollX() {
        return Math.max(0,
                Math.min(player.getX()-SCREEN_WIDTH / 2,
                        player.getMap().getWidth() - SCREEN_WIDTH));
    }

    /**
     * limits vertical scrolling
     * @return
     */
    public int getScrollY() {
        return Math.max(0, Math.min(player.getY()-SCREEN_HEIGHT / 2, player.getMap().getHeight() - SCREEN_HEIGHT));
    }
}
