package rla.states.rendering;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.Entity;
import rla.entities.items.Item;
import rla.states.GameState;
import rla.states.InventoryState;

import java.util.ArrayList;
import java.util.List;

public class InventoryRenderer extends SingleListRenderer {
    public final int ITEMS_PER_PAGE = 7; // needs to be doable relative to screen/box size

    @Override
    String getPanelTitle() {
        return "Inventory - [" + ((InventoryState)state).getVerb() + "]";
    }

    @Override
    public void use(Entity item) {
        ((InventoryState)state).use((Item)item);
    }

    @Override
    public List<Entity> checkValidItems() {
        List<Item> items = ((InventoryState)state).checkValidItems();
        return new ArrayList<>(items);
    }

    @Override
    String getItemDisplayInfo(Entity item) {
        Item i = (Item)item;
        return i.getGlyph() + " - " + i.getName();
    }

    public InventoryRenderer(Screen baseScreen, GameState state) {
        super(baseScreen, state);
    }

}
