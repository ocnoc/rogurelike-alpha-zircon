package rla.states.rendering;

import org.hexworks.zircon.api.Components;
import org.hexworks.zircon.api.Layers;
import org.hexworks.zircon.api.Positions;
import org.hexworks.zircon.api.Sizes;
import org.hexworks.zircon.api.component.Button;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.component.Component;
import org.hexworks.zircon.api.component.Panel;
import org.hexworks.zircon.api.data.Position;
import org.hexworks.zircon.api.graphics.Layer;
import org.hexworks.zircon.api.screen.Screen;
import rla.displays.TerminalDisplay;
import rla.entities.Entity;
import rla.entities.items.Item;
import rla.states.GameState;
import rla.states.InventoryState;
import rla.states.PlayState;

import java.util.ArrayList;
import java.util.List;

public abstract class SingleListRenderer implements StateRenderer {
    Screen baseScreen;
    GameState state;

    private final ColorTheme theme = TerminalDisplay.COMPONENT_THEME;
    public final int ITEMS_PER_PAGE = 7; // needs to be doable relative to screen/box size
    private final int PANEL_WIDTH = 30;
    private final String DEFAULT_VERB = "noverb";

    private Layer layer;
    private Panel itemBox;
    private List<Entity> validItems;

    private int page;
    private List<Component> lines;

    abstract String getPanelTitle();
    abstract public void use(Entity item);
    abstract public List<Entity> checkValidItems();
    abstract String getItemDisplayInfo(Entity item);

    public SingleListRenderer(Screen baseScreen, GameState state) {
        this.baseScreen = baseScreen;
        this.state = state;
        this.initializeLayer();
    }

    private void initializeLayer() {

        itemBox = Components.panel().
                wrapWithBox(true).
                withTitle(this.getPanelTitle()).
                withPosition(Positions.create((baseScreen.getWidth()/2) - (PANEL_WIDTH/2), 1)).
                withSize(Sizes.create(PANEL_WIDTH, baseScreen.getHeight()- PlayState.SCREEN_OFFSET - 1)). // testing what numbers work and which will kill my code
                build();
        itemBox.applyColorTheme(theme);
        //baseScreen.pushLayer(itemBox);
        baseScreen.addComponent(itemBox);
    }

    public void render() {
        displayItems();
    }
    private void displayItems() {
        itemBox.detachAllComponents();
        lines = new ArrayList<>();
        validItems = checkValidItems();
        for(int i = page*ITEMS_PER_PAGE ; i < (page+1)*ITEMS_PER_PAGE ; i++) {
            if(checkIndex(i) && validItems.get(i) != null) {
                createItemLabel(validItems.get(i), i - (page * ITEMS_PER_PAGE));
            }
        }
        itemBox.addComponent(Components.label().
                withText("Page " + (page+1) + "/" + (calculateMaxPages()+1)).
                withSize(Sizes.create(10, 1)).
                withPosition(Positions.create(itemBox.getWidth()-12, itemBox.getHeight()-3)).build());
    }

    private boolean checkIndex(int index) {
        try {
            validItems.get(index);
            return true;
        }
        catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    private void createItemLabel(Entity item, int i) {
        char keyMap = (char)('a' + i);
        Position pos = Positions.offset1x1();
        if(i > 0 ) {
            pos = Positions.create(1, i+1);
        }
        Button itemLabel = Components.button().
                withText("[" + keyMap + "] " + getItemDisplayInfo(item)).
                withSize(Sizes.create(itemBox.getWidth()-3, 1)).
                withPosition(pos).
                build();
        itemLabel.applyColorTheme(theme);
        itemLabel.onMouseClicked(a -> {
            use(item);
            state.render(baseScreen);
        });
        itemBox.addComponent(itemLabel);
        lines.add(itemLabel);
    }

    public void changePage(int page) {
        itemBox.detachAllComponents();
        lines.clear();
        if(page > calculateMaxPages() ) {
            this.page = 0;
        }
        this.page = page;
    }


    public int calculateMaxPages() {
        return (validItems.size() - 1) / ITEMS_PER_PAGE;
    }

    public void closeMenu() {
        //baseScreen.popLayer();
        baseScreen.removeComponent(itemBox);
    }
}
