package rla.states;

import org.hexworks.zircon.api.*;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.grid.TileGrid;
import org.hexworks.zircon.api.input.InputType;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;

public class StartState implements GameState {
    private int screenWidth;
    private int screenHeight;
    private Screen startScreen;
    private TileGrid terminal;
    private ColorTheme theme = ColorThemes.techLight();

    public StartState(TileGrid terminal) {
        screenHeight = terminal.getSize().getHeight();
        screenWidth = terminal.getSize().getWidth();
        this.terminal = terminal;
        startScreen = Screens.createScreenFor(terminal);
        startScreen.display();
        startScreen.addComponent(Components.label().withText("Enter to start"));
        startScreen.applyColorTheme(theme);
    }

    @Override
    public boolean renderBelowState() {
        return false;
    }

    @Override
    public void render(DrawSurface terminal) {

    }



    @Override
    public void update(KeyStroke key) {
        if(key.inputType() == InputType.Enter) {
            startScreen.clear();
            StateManager.setState(new PlayState(screenWidth, screenHeight, terminal));
        }
    }
}
