package rla.states;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.RLAComponent;
import rla.entities.actors.Actor;
import rla.entities.actors.components.HungerComponent;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.entities.items.Items;
import rla.entities.items.components.FoodComponent;

public class InventoryEatState extends InventoryState{
    public InventoryEatState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        super(actor, items, priorState, screen);
    }

    @Override
    public String getVerb() {
        return "eat";
    }

    @Override
    public boolean isValid(Item item) {
        return item.getComponent(FoodComponent.class) != null;
    }

    @Override
    public GameState use(Item item) {
        RLAComponent hunger = actor.getComponent(HungerComponent.class);
        FoodComponent food = ((FoodComponent)item.getComponent(FoodComponent.class));
        if(hunger != null) {
            ((HungerComponent)hunger).modifyFullness(food.getNutritionValue());
            Items.removeItem(item.getParent(), item);
            actor.doAction("eat the %s", item.getName());
        }
        return this;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }
}
