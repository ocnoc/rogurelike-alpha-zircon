package rla.states;

import rla.entities.Entity;

public interface SelectableList {
    void use(int page);
    void changePage(int page);
}
