package rla.states;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.actors.Actor;
import rla.entities.actors.components.BodyComponent;
import rla.entities.actors.components.bodyparts.BodyPart;
import rla.entities.items.Inventory;
import rla.entities.items.Item;

public class InventoryWearState extends InventoryState {
    private BodyPart part;
    public InventoryWearState(Actor actor, Inventory items, GameState priorState, Screen screen, BodyPart part) {
        super(actor, items, priorState, screen);
        this.part = part;
    }

    @Override
    public String getVerb() {
        return "wear";
    }

    @Override
    public boolean isValid(Item item) {
        return true;
    }

    @Override
    public GameState use(Item item) {
        ((BodyComponent)actor.getComponent(BodyComponent.class)).equip(part, item);
        return this;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }
}
