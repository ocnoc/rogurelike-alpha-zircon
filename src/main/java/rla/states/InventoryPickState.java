package rla.states;

import org.hexworks.zircon.api.screen.Screen;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.entities.items.Items;
import rla.states.rendering.InventoryRenderer;

public class InventoryPickState extends InventoryState {


    public InventoryPickState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        super(actor, items, priorState, screen);
    }

    @Override
    public String getVerb() {
        return "pick up";
    }

    @Override
    public boolean isValid(Item item) {
        return item.isDroppable();
    }

    @Override
    public GameState use(Item item) {
        actor.getItem(item);
        return priorState;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }



}
