package rla.states;

import org.hexworks.zircon.api.graphics.DrawSurface;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.screen.Screen;
import rla.actions.Action;
import rla.actions.Actions;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;
import rla.states.rendering.InventoryRenderer;

import java.util.ArrayList;
import java.util.List;

public class InventoryDropState extends InventoryState{

    public InventoryDropState(Actor actor, Inventory items, GameState priorState, Screen screen) {
        super(actor, items, priorState, screen);
    }

    @Override
    public String getVerb() {
        return "drop";
    }

    @Override
    public boolean isValid(Item item) {
        return item.isDroppable();
    }

    @Override
    public GameState use(Item item) {
        actor.drop(item);
        return this;
    }

    @Override
    public GameState getPriorState() {
        return priorState;
    }
}
