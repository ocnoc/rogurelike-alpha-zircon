package rla;

import rla.actions.Actions;
import rla.datahandling.ExternalDataHub;
import rla.entities.actors.ActorFactory;
import rla.entities.actors.ActorLibrary;
import rla.entities.actors.components.races.Races;
import rla.entities.items.ItemLibrary;
import rla.tiles.TileLibrary;
import rla.utilities.MessageInterfaceLocator;
import rla.utilities.MessageLog;

public class Game {
    public static void init() {
        ExternalDataHub.getHub().init();
        MessageInterfaceLocator.provide(new MessageLog());
        Actions.getActions().initializeActions();
        TileLibrary.init();
        ItemLibrary.init();
        ActorLibrary.init();
        Races.initRaces();
    }
}
