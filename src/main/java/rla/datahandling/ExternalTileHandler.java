package rla.datahandling;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import rla.tiles.Tile;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ExternalTileHandler {
    private Gson gson;
    private String TILE_DATA_LOCATION = "/tiles.json";
    private ClassLoader loader;
    private List<Tile> tiles;

    ExternalTileHandler(String dataPath) {
        gson = new GsonBuilder().setPrettyPrinting().create();
        loader = getClass().getClassLoader();
        TILE_DATA_LOCATION = dataPath + TILE_DATA_LOCATION;
    }

    public void saveTiles(List<Tile> tiles) {
        String json = gson.toJson(tiles);
        File file = new File(Objects.requireNonNull(loader.getResource(TILE_DATA_LOCATION)).getPath());
        try {
            FileWriter f = new FileWriter(file);
            f.write(json);
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public List<Tile> loadTileData() {
        loadTileDataFile();
        return tiles;
    }

    private boolean loadTileDataFile() {
        FileReader file;
        try {
            file = new FileReader(Paths.get(TILE_DATA_LOCATION).toAbsolutePath().toString());
            System.out.println("Loaded from " + Paths.get(TILE_DATA_LOCATION).toAbsolutePath().toString());
            JsonReader reader = new JsonReader(file);
            tiles = new ArrayList<>();
            loadTileJson(reader);
            reader.close();
        }
        catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void loadTileJson(JsonReader reader) {
        try {
            reader.beginArray();
            while(reader.hasNext()) {
                tiles.add(parseTile(reader));
            }
            reader.endArray();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Tile parseTile(JsonReader reader) {
        char glyph = '@';
        int[] color = new int[]{0, 0, 0};
        byte features = 0;

        try {
            reader.beginObject();
            while(reader.hasNext()) {
                switch (reader.nextName()) {
                    case "glyph": glyph = reader.nextString().charAt(0); break;
                    case "features": features = (byte)reader.nextInt(); break;
                    case "color": color = parseColor(reader); break;
                    default: reader.skipValue();
                }
            }
            reader.endObject();
            return new Tile(glyph, color, features);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int[] parseColor(JsonReader reader) {
        int[] colors = new int[]{0, 0, 0};
        try {
            reader.beginObject();
            while(reader.hasNext()) {
                switch(reader.nextName()) {
                    case "r": colors[0] = reader.nextInt(); break;
                    case "g": colors[1] = reader.nextInt(); break;
                    case "b": colors[2] = reader.nextInt(); break;
                    default: reader.skipValue();
                }
            }
            reader.endObject();
            return colors;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new int[0];
    }


}
