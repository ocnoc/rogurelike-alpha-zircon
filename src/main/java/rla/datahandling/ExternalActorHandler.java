package rla.datahandling;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import rla.entities.actors.Actor;
import rla.entities.ai.CreatureAI;
import rla.entities.ai.GenericAI;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

public class ExternalActorHandler {
    private Gson gson;
    private ClassLoader loader;
    private String ACTOR_DATA_LOCATION = "/actors.json";
    private final String AI_CLASS_PATH = "rla.entities.ai.";

    ExternalActorHandler(String dataPath) {
        gson = new GsonBuilder().setPrettyPrinting().create();
        loader = getClass().getClassLoader();
        ACTOR_DATA_LOCATION = dataPath + ACTOR_DATA_LOCATION;
    }

    public void saveActors(List<Actor> actors) {
        gson = new Gson().newBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(actors);
        File file = new File(Objects.requireNonNull(loader.getResource(ACTOR_DATA_LOCATION).getPath()));
        try {
            FileWriter f = new FileWriter(file);
            f.write(json);
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public List<Actor> loadActorData() {
        return loadActorDataFile();
    }

    private List<Actor> loadActorDataFile() {
        FileReader file;
        try {
            file = new FileReader(Paths.get(ACTOR_DATA_LOCATION).toAbsolutePath().toString()); // if we're a jar our data is externalized
            JsonReader reader = new JsonReader(file);
            List<Actor> actors = new ArrayList<>();
            reader.beginArray();
            while(reader.hasNext()) {
                actors.add(parseActor(reader));
            }
            reader.endArray();
            reader.close();
            return actors;

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return null;
    }

    private Actor parseActor(JsonReader reader) {
        int maxhp = -1;
        int atk = -1;
        int def = -1;

        char glyph = 'X';
        int[] color = new int[]{0, 0, 0};
        String name = "noname";
        Class<? extends CreatureAI> ai = GenericAI.class;
        int visionRadius = 8;

        try {
            reader.beginObject();
            while(reader.hasNext()) {
                String readerName = reader.nextName();
                switch (readerName) {
                    case "maxhp":
                        maxhp = reader.nextInt();
                        break;
                    case "atk":
                        atk = reader.nextInt();
                        break;
                    case "def":
                        def = reader.nextInt();
                        break;
                    case "glyph":
                        glyph = reader.nextString().charAt(0);
                        break;
                    case "color":
                        color = parseColor(reader);
                        break;
                    case "name":
                        name = reader.nextString();
                        break;
                    case "ai":
                        String className = AI_CLASS_PATH + reader.nextString();
                        Class<?> loadedClass = Class.forName(className);
                        if (CreatureAI.class.isAssignableFrom(loadedClass)) { // no need for else, default is genericai
                            ai = loadedClass.asSubclass(CreatureAI.class);
                        }
                        break;
                    case "visionRadius":
                        visionRadius = reader.nextInt();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
            return new Actor(null, name, glyph, color, maxhp, atk, def, ai, visionRadius);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int[] parseColor(JsonReader reader) {
        try {
            int r = -1;
            int g = -1;
            int b = -1;
            reader.beginObject();
            while(reader.hasNext()) {
                String rname = reader.nextName();
                switch (rname) {
                    case "r":
                        r = reader.nextInt();
                        break;
                    case "g":
                        g = reader.nextInt();
                        break;
                    case "b":
                        b = reader.nextInt();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
            return new int[]{r, g, b};
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new int[]{0, 0, 0};
    }
}
