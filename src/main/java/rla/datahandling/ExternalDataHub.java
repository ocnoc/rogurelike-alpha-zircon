package rla.datahandling;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;

public class ExternalDataHub {
    public static String DATA_FOLDER_PATH = "data";
    public static ExternalDataHub dataHub;

    public static ExternalDataHub getHub() {
        if(dataHub == null) {
            dataHub = new ExternalDataHub();
        }
        return dataHub;
    }

    public ExternalActorHandler getActorLoader() {
        return new ExternalActorHandler(DATA_FOLDER_PATH);
    }

    public ExternalTileHandler getTileLoader() {
        return new ExternalTileHandler(DATA_FOLDER_PATH);
    }

    public void init() {
        ClassLoader loader = getClass().getClassLoader();
        if (loader.getResource(DATA_FOLDER_PATH) != null && new File(loader.getResource(DATA_FOLDER_PATH).getPath()).exists()) { // Nasty checking to see if we're in jar form.
            DATA_FOLDER_PATH = loader.getResource(DATA_FOLDER_PATH).getPath(); // if we're running from source our data is a resource
        }
        else {
            DATA_FOLDER_PATH = Paths.get(DATA_FOLDER_PATH).toAbsolutePath().toString(); // if we're a jar our data is externalized
        }
    }
}
