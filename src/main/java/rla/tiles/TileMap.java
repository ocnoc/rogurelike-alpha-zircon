package rla.tiles;

public class TileMap {
    Tile[][] tiles;
    private final char FLOOR_CHAR = '.';
    private final char WALL_CHAR = '#';

    public enum Direction {
        NORTH(0, -1),
        SOUTH(0, 1),
        WEST(-1, 0),
        EAST(1, 0);

        int[] directionalValue;

        Direction(int x, int y) {
            this.directionalValue = new int[2];
            this.directionalValue[0] = x;
            this.directionalValue[1] = y;
        }
    }

    public TileMap(int width, int height) {
        tiles = new Tile[width][height];
    }

    public void paint(int x, int y, Tile tile) {
        if(isOob(x, y)) {
            return;
        }
        tiles[x][y] = tile;
    }

    public void paintFloor(int x, int y) {
        paint(x, y, TileLibrary.getFloor());
    }

    public void paintWall(int x, int y) {
        paint(x, y, TileLibrary.getWall());
    }

    public int getWidth() {
        return tiles.length;
    }

    public int getHeight() {
        return tiles[0].length;
    }

    /**
     * Calculates a char array where every char[x][y] is either a '.' if the corresponding tile is transparent, or a '#' otherwise
     * @return vision char map
     */
    public char[][] getVisionMap() {
        char[][] visionMap = new char[getWidth()][getHeight()];
        for(int x=0;x<getWidth();x++) {
            for(int y=0;y<getHeight();y++) {
                if(tiles[x][y].isTransparent()) {
                    visionMap[x][y] = FLOOR_CHAR;
                }
                else {
                    visionMap[x][y] = WALL_CHAR;
                }
            }
        }
        return visionMap;
    }

    /**
     * Calculates a char array where every char[x][y] is either a '.' if the corresponding tile is passable, or a '#' otherwise
     * @return passability char map
     */
    public char[][] getPassabilityMap() {
        char[][] passabilityMap = new char[getWidth()][getHeight()];
        for(int x=0;x<getWidth();x++) {
            for(int y=0;y<getHeight();y++) {
                if(tiles[x][y].isPassable()) {
                    passabilityMap[x][y] = FLOOR_CHAR;
                }
                else {
                    passabilityMap[x][y] = WALL_CHAR;
                }
            }
        }
        return passabilityMap;
    }

    public boolean isPassable(int x, int y) {
        if(isOob(x, y)) {
            return false;
        }
        return tiles[x][y].isPassable();
    }

    public void paintRect(int x1, int y1, int x2, int y2, int tileID, int padding) {
        if(x1 > x2) { // swap these two if y1 < y2
            int temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if(y1 > y2) {
            int temp = y1;
            y1 = y2;
            y2 = temp;
        }
        for(int x=x1 + padding; x<=x2 - padding; x++) {
            for(int y=y1 + padding;y<=y2 - padding;y++) {
                paint(x, y, TileLibrary.getTile(tileID));
            }
        }
    }

    public void fillAll(int tileID) {
        for(int x=0;x<getWidth();x++) {
            for(int y=0;y<getHeight();y++) {
                tiles[x][y] = TileLibrary.getTile(tileID);
            }
        }
    }

    public Tile getTile(int x, int y) {
        if(isOob(x, y)) {
            return TileLibrary.getTile(TileLibrary.TileType.OOB);
        }
        return tiles[x][y];
    }

    public boolean hasFeature(int x, int y, byte f) {
        return tiles[x][y].hasFeature(f);
    }

    /**
     * Finds if a tile has a passable boundary on 1 side of it, and returns the direction opposite that side
     * @param x
     * @param y
     * @return
     */
    public int[] checkAir(int x, int y) {
        if(isPassable(x, y)) {
            return null; // not a wall, it's passable
        }
        int[] emptySpace = new int[2];
        int numSpaces = 0;
        // if we have a empty space west
        if(isPassable(x-1, y)) {
            numSpaces++;
            emptySpace = Direction.EAST.directionalValue; // dig east
        }
        if(isPassable(x+1, y)) {
            numSpaces++;
            emptySpace = Direction.WEST.directionalValue; // dig west
        }
        if(isPassable(x, y-1)) {
            numSpaces++;
            emptySpace = Direction.SOUTH.directionalValue;
        }
        if(isPassable(x, y+1)) {
            numSpaces++;
            emptySpace = Direction.NORTH.directionalValue;
        }
        if(numSpaces != 1) {
            return null;
        }
        return emptySpace;
    }

    public boolean isOob(int x, int y) {
        return (x < 0 || y < 0 || x >= getWidth() || y >= getHeight());
    }

    public boolean areaImpassable(int x1, int y1, int x2, int y2, int padding) {
        if(x1 > x2) { // swap these two if y1 < y2
            int temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if(y1 > y2) {
            int temp = y1;
            y1 = y2;
            y2 = temp;
        }
        for(int i=x1 + padding;i<=x2 - padding;i++) {
            for(int y=y1 + padding;y<=y2 - padding;y++) {
                if (isPassable(i, y) || isOob(i, y)) {
                    return false;
                }
            }
        }
        return true;
    }

}
