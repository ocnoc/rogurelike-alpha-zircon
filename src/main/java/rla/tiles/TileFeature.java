package rla.tiles;

public enum TileFeature {
    PASSABLE((byte)1),
    DOOR((byte)2),
    DIGGABLE((byte)4),
    VISION_BLOCKING((byte)8);

    byte value;

    TileFeature(byte b) {
        this.value = b;
    }
}
