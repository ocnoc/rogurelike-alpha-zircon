package rla.tiles;


import rla.datahandling.ExternalDataHub;

import java.util.ArrayList;
import java.util.List;

public class TileLibrary {
    private static List<Tile> tileLibrary = new ArrayList<>();

    public enum TileType {
        FLOOR(0),
        WALL(1),
        OOB(2),
        DOOR(3);

        public int value;

        TileType(int value) {
            this.value = value;
        }
    }

    public static void init() {
        // index 0 is floor, 1 is wall, 2 is oob
        tileLibrary = ExternalDataHub.getHub().getTileLoader().loadTileData();
    }

    public static Tile getTile(int index) {
        try {
            return (Tile)tileLibrary.get(index).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    public static Tile getTile(TileType index) {
        try {
            return (Tile)tileLibrary.get(index.value).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    public static Tile getFloor() {
        return getTile(TileType.FLOOR);
    }

    public static Tile getWall() {
        return getTile(TileType.WALL);
    }

    public static int addTile(Tile tile) {
        tileLibrary.add(tile);
        return tileLibrary.indexOf(tile);
    }

}
