package rla.tiles;

import rla.entities.ItemContainer;
import rla.entities.actors.Actor;
import rla.entities.items.Inventory;
import rla.entities.items.Item;

public class Tile implements Cloneable, ItemContainer {
    private char glyph;
    private int[] color;
    private byte features;
    private Inventory items;

    public int[] getColor() {
        return color;
    }
    public void setColor(int[] rgb) {
        this.color = rgb;
    }

    public Tile(char glyph, int[] color, byte features) {
        this.glyph = glyph;
        this.color = color;
        this.features = features;
        this.items = new Inventory();
    }

    public boolean hasFeature(byte feature) {
        return (features & feature) == feature;
    }

    public boolean isPassable() {
        return hasFeature(TileFeature.PASSABLE.value);
    }

    public boolean isDoor() { return hasFeature(TileFeature.DOOR.value); }

    public boolean isDiggable() {
        return hasFeature(TileFeature.DIGGABLE.value);
    }

    public boolean isTransparent() {
        return !(hasFeature(TileFeature.VISION_BLOCKING.value));
    }

    public boolean onDig(Actor digger) {
        this.setFeature(TileFeature.PASSABLE, true);
        this.setFeature(TileFeature.DIGGABLE, false);
        this.setFeature(TileFeature.VISION_BLOCKING, false);
        this.setGlyph(TileLibrary.getFloor().getGlyph());
        digger.doAction("dig through the wall");
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        Tile clone = (Tile) super.clone();
        clone.items = new Inventory();
        return clone;
    }

    public void setGlyph(char c) {
        this.glyph = c;
    }

    public boolean onBump(Actor bumper) {
        if(hasFeature(TileFeature.DOOR.value)) {
            this.setFeature(TileFeature.PASSABLE, true);
            this.setFeature(TileFeature.VISION_BLOCKING, false);
            this.setGlyph(TileLibrary.getFloor().glyph); // set our door to look like a floor
            bumper.doAction("bump down the door");
            return true;
        }
        return false;
    }

    public void setFeature(TileFeature feature, boolean active) {
        byte f = feature.value;
        if(active) {
            this.features = (byte)(features | f);
        }
        else {
            this.features = (byte)(features ^ f);
        }
    }

    public char getGlyph() {
        return glyph;
    }

    @Override
    public Inventory getInventory() {
        return items;
    }

}
