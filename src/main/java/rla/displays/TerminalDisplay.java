package rla.displays;

import org.hexworks.zircon.api.*;
import org.hexworks.zircon.api.application.Application;
import org.hexworks.zircon.api.component.ColorTheme;
import org.hexworks.zircon.api.grid.TileGrid;
import org.hexworks.zircon.api.input.KeyStroke;
import org.hexworks.zircon.api.resource.BuiltInGraphicTilesetResource;
import org.hexworks.zircon.api.resource.TilesetResource;
import rla.Game;
import rla.states.StartState;
import rla.states.StateManager;


public class TerminalDisplay {


    private TileGrid terminal;
    private Application app;

    private final TilesetResource TILESET = CP437TilesetResources.rogueYun16x16();//CP437TilesetResources.rexPaint16x16();
    private final BuiltInGraphicTilesetResource GRAPHIC_TILESET = BuiltInGraphicTilesetResource.NETHACK_16X16;
    public static final ColorTheme COMPONENT_THEME = ColorThemes.zenburnVanilla();
    private final int SCREEN_WIDTH = 50;
    private final int SCREEN_HEIGHT = 40;

    public static void main(String[] args) {
        new TerminalDisplay();
    }

    /**
     * Creates a new Application object
     */
    public TerminalDisplay() {
        app = SwingApplications.startApplication(AppConfigs.newConfig().
                withDefaultTileset(TILESET).
                withSize(Sizes.create(SCREEN_WIDTH, SCREEN_HEIGHT)).
                withDebugMode(false).
                build());

        terminal = app.getTileGrid();

        terminal.onInput(key -> {
            if (key.isKeyStroke()) {
                KeyStroke keyStroke = key.asKeyStroke().get();
                receiveKeyPress(keyStroke);
            }
        });

        StateManager.setState(new StartState(terminal));



        Game.init();
        repaint();
    }

    public void repaint() {
        StateManager.render(terminal);
    }

    public void receiveKeyPress(KeyStroke key) {
        StateManager.update(key);
        repaint();
    }
}
